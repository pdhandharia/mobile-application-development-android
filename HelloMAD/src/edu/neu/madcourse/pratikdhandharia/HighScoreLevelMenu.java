package edu.neu.madcourse.pratikdhandharia;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.GridView;

public class HighScoreLevelMenu extends Activity{
	
	GridView gridView;
	public static final String TAG = "HighScoreLevelMenu";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		Log.d(TAG, "Number of levels: "+ Ngage.listOfLevels.levels.size());
		setContentView(R.layout.highscore_levelmenu);
		gridView = (GridView)findViewById(R.id.gridhs);
		gridView.setAdapter(new ButtonAdapter(this,true));
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

}
