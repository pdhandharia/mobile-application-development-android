package edu.neu.madcourse.pratikdhandharia;

import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v4.view.ViewPager.LayoutParams;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;

public class ButtonAdapter extends BaseAdapter implements OnClickListener{

	private Context mContext;
	private boolean ishighscore;
	
	public ButtonAdapter(Context c,Boolean b) { 
		mContext = c;
		ishighscore = b;
	}
	
	public int getCount() {
		// TODO Auto-generated method stub
		if(Ngage.listOfLevels !=null && Ngage.listOfLevels.levels !=null)
			return Ngage.listOfLevels.levels.size();
		else return 0;
	}

	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		Button btn;
		if (convertView == null)
		{
			btn = new Button(mContext);
			btn.setLayoutParams(new GridView.LayoutParams(NgageMainActivity.width, LayoutParams.WRAP_CONTENT));
			btn.setBackgroundResource(R.drawable.button);
			//btn.setPadding(10, 10, 10, 10);
		}
		else
		{
			btn = (Button) convertView;
		}
		//btn.setPadding(10, 10, 10, 10);
		btn.setText("Level "+Ngage.listOfLevels.levels.get(position).level_number);
		if (position <= NgageMainActivity.user.scores.size())
			btn.setEnabled(true);
		else
			btn.setEnabled(false);
		btn.setId(position);
		btn.setTextColor(Color.WHITE);
		btn.setTag(Integer.valueOf((Ngage.listOfLevels.levels.get(position).level_number)));
		btn.setOnClickListener(this);
		return btn;
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		int level_number = (Integer)v.getTag();
		//Ngage.currentLevel = level_number;
		NgageMainActivity.user.current_level = level_number;
		//lvlmenu.startGame();
		Intent i;
		if(ishighscore == true)
		{
			i = new Intent(mContext, Ngage_HighScore.class);
		}
		else
		{
			//i = new Intent(mContext, NewNGageGame.class);
			i = new Intent(mContext, NewNgageGameInstructions.class);
		}
		mContext.startActivity(i);
	}

	
}

