package edu.neu.madcourse.pratikdhandharia;

import edu.neu.mobileclass.apis.KeyValueAPI;
import edu.neu.mobileClass.*;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.view.Display;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
	
	public static final String USER_KEY = "user";
	public static final String USERNAME = "username";
//	public static final String NEWUSER = "newuser";
//	public static final String NGAGEUSER = "ngageuser";
//	public static final String NEWUSERPASSWORD = "newuserpassword";
//	public static final String NGAGEUSERPASSWORD = "ngageuserpassword";
	final Context context = this;
	public static String username = "";
	//public static User user;
	public static String imei;
	//public static int width;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PhoneCheckAPI.doAuthorization(this);
        setContentView(R.layout.activity_main);
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        imei = telephonyManager.getDeviceId();
       // user = new User();
        setTitle("Pratik Dhandharia");
        
//        Display display = getWindowManager().getDefaultDisplay();
//        width = (int) ((display.getWidth()/2)*0.8);
        //button click for Team member
        final Button button = (Button) findViewById(R.id.btnTeamMember);
        button.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				try
				{
				StringBuilder s = new StringBuilder();
				s.append("Team Member 1: Pratik Dhandharia\n");
				s.append("Team Member 1 Email: dhandharia.p@husky.neu.edu\n");
				s.append("Team Member 2: Prachi Gawade\n");
				s.append("Team Member 2 Email: gawade.p@husky.neu.edu\n");
				String p = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
				s.append("Version: " + p + "\n");
				
				String id = new String();
				TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
				id = telephonyManager.getDeviceId();				
				s.append("Unique ID: " + id);
				
				AlertDialog a = new AlertDialog.Builder(context).create();
				a.setMessage(s);
				a.setTitle("Team Members");
				a.show();
				}
				catch(Exception e){
					throw new RuntimeException(e);
				}
			}
		});
    
        //button click for sudoku
        final Button buttonsudoku = (Button) findViewById(R.id.btnSudoku);
        buttonsudoku.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Intent i1 = new Intent(context, Sudoku.class);
				startActivity(i1);
			}
		});
        
        //button click for Boggle
        final Button btnboggle = (Button) findViewById(R.id.btnboggle);
        btnboggle.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				Intent i2 = new Intent(context,Boggle.class);
				startActivity(i2);
				
			}
		});
        
      //button click for PersistentBoggle
        final Button btnpersboggle = (Button) findViewById(R.id.btnpersistentboggle);
        btnpersboggle.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				if (username.equals("") && getSharedPreferences(USER_KEY, Activity.MODE_PRIVATE).contains(USERNAME))
				{
					username = getSharedPreferences(USER_KEY, Activity.MODE_PRIVATE)
							.getString(USERNAME,"");
				}
			    if (username.equals(""))
			    {
			    	Intent i3 = new Intent(context,Register.class);
					startActivity(i3);
			    }
			    else 
			    {
			    	KeyValueAPI.put("Prachi_Pratik", "neu12345", username+"_online", "true");
			    	Intent i3 = new Intent(context,PersistentBoggle.class);
					startActivity(i3);
			    }
			}
		});
        
      //button click for Trickiest part
        final Button btntrickiestpart = (Button) findViewById(R.id.btntrickiestpart);
        btntrickiestpart.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Intent i2 = new Intent(context,TrickiestPart.class);
				startActivity(i2);
			}
		});
      
        
      
        final Button btnngage = (Button) findViewById(R.id.btnngage);
        btnngage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				//call the description call
				Intent i = new Intent(context,NgageDescription.class);
				startActivity(i);
			}
		});
        
        //button click for Create Error
        final Button btnerror = (Button) findViewById(R.id.btnCreateError);
        btnerror.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v)  {
				try{
					Integer i = 10;
					i = i / 0;
				}
				catch(Exception e){
					throw new RuntimeException(e);
				}
			}
		});
        
        final Button quit = (Button) findViewById(R.id.btnQuit);
        quit.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
    
}
