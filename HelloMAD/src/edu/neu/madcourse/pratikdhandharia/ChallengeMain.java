package edu.neu.madcourse.pratikdhandharia;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ChallengeMain extends Activity{

	TextView txtTotalChallenges, txtChallengesWon, txtChallengeLost;
	Button btnbackChallenge;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.challenge_main);
		
		//MainActivity.user.deleteChallenged();
		
//		NgageMainActivity.user.totalChallenges = getPreferences(Context.MODE_PRIVATE).getInt("totalChallenges", 0);
//		NgageMainActivity.user.totalWon = getPreferences(Context.MODE_PRIVATE).getInt("totalwon", 0);
		
		txtChallengeLost = (TextView) findViewById(R.id.ChallengesLost);
		txtChallengesWon = (TextView) findViewById(R.id.ChallengesWon);
		txtTotalChallenges = (TextView) findViewById(R.id.TotalChallenges);	
		
		txtTotalChallenges.setText("Total Challenges : "+NgageMainActivity.user.totalChallenges);
		txtChallengesWon.setText("Challenges Won : "+NgageMainActivity.user.totalWon);
		txtChallengeLost.setText("Challenges Lost : "+(NgageMainActivity.user.totalChallenges - NgageMainActivity.user.totalWon));

		btnbackChallenge = (Button) findViewById(R.id.btnbackchallenge);
		btnbackChallenge.setText("Back");
		btnbackChallenge.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
	}
	
//	public void saveTotalChallenges(int total)
//	{
//		getPreferences(Context.MODE_PRIVATE).edit().putInt("totalChallenges", total).commit();
//	}
//	public void saveChallengesWon(int won)
//	{
//		getPreferences(Context.MODE_PRIVATE).edit().putInt("totalwon", won).commit();
//	}
}
