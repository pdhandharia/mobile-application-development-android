package edu.neu.madcourse.pratikdhandharia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;

import com.everyfit.wockets.data.AccelerationData;
import com.everyfit.wockets.logging.Logger;
import com.everyfit.wockets.sensors.Sensor;
import edu.neu.mobileclass.apis.KeyValueAPI;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class NewNGageGame extends Activity implements DataServiceInterface,OnClickListener{
	
	private static final String TAG = "NGageNewGame";
	private static final int UpdateTimer = 1;
	private static final int UpdateOnlineScore = 2;
	private static final int FinishTimer = 3;
	private static final int showToast = 4;
	private static final String TimeRemaining = "timeRemaining";
	private static final String Score = "score";
	private static final String LevelNo = "levelnumber";
	Boolean challengeGame = false;
	protected static final String ONLINESCORE = "onlinescore";

	int block_height, block_width;
	TextView userHighScore, onlineHighScore, levelNumber, currentScore, timeRemaining;
	Button btnquit;
	Handler handler;
	Mycountdowntimer mytimer;
	Intent i;
	Level thisLevel;
	Boolean sensor1x,sensor1y,sensor1z,sensor2x,sensor2y,sensor2z;
	MediaPlayer mp,mp_finish;

	FinishGame fg;
	TableLayout tl;
	private String online_high_score;
	LoadData ld;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_game);
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		challengeGame = getIntent().getBooleanExtra("CHALLENGE", false);

		if (challengeGame == true)
		{
			thisLevel = Ngage.listOfLevels.levels.get(NgageMainActivity.user.challenges.get(0).level-1);
			NgageMainActivity.user.current_level = thisLevel.level_number;
		}
		else
			thisLevel = Ngage.listOfLevels.levels.get(NgageMainActivity.user.current_level-1);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
//		pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//		wl = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "My Tag");
//		wl.acquire();-

		
		ld = new LoadData(this);
		ld.execute();
	}
	
	public void initializeGame()
	{
		
//		Log.d("New NGage Game", ""+NgageMainActivity.user.current_level + NgageMainActivity.user.username);
//		Log.d("Challenge Request", ""+challengeGame);
		

		fg = new FinishGame(this);
		
		Display display = getWindowManager().getDefaultDisplay();
		block_height = display.getHeight() / 13;
		block_width = display.getWidth() / 2;
		
		CopyOfCollectData.addListener(this);
		i = new Intent(this, CopyOfCollectData.class);
		
		sensor1x = false;
		sensor1y = false;
		sensor1z = false;
		sensor2x = false;
		sensor2y = false;
		sensor2z = false;
		
		mp = MediaPlayer.create(this, R.raw.gridclick);
		mp_finish = MediaPlayer.create(this, R.raw.sms_bell);
		
		handler = new Handler(){
			
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				switch(msg.what)
				{
				case UpdateTimer:
					timeRemaining.setText("Time Remaining : " + msg.getData().getString(TimeRemaining) + " seconds");
					break;
				case showToast:
					Toast.makeText(getApplicationContext(), msg.getData().getString("message"), Toast.LENGTH_LONG).show();
					break;
					
				}
			}		
		};
		
		if(thisLevel != null)
		{		
			NgageMainActivity.user.current_score = 0;
			userHighScore = (TextView) findViewById(R.id.txtuserhighscore);
			onlineHighScore = (TextView) findViewById(R.id.txtonlinehighscore);
			levelNumber = (TextView) findViewById(R.id.txtlevelnumber);
			currentScore = (TextView) findViewById(R.id.txtcurrentscore);
			timeRemaining = (TextView) findViewById(R.id.txttimeremaining);
			btnquit = (Button) findViewById(R.id.btnquit);
			
			userHighScore.setWidth(block_width*2);
			onlineHighScore.setWidth(block_width*2);
			levelNumber.setWidth(block_width * 2);
			currentScore.setWidth(block_width * 2);
			timeRemaining.setWidth(block_width * 2);
			btnquit.setWidth(block_width/2);
			
			btnquit.setBackgroundResource(R.drawable.button);
			btnquit.setTextColor(Color.WHITE);
			
			userHighScore.setHeight(block_height*2);
			onlineHighScore.setHeight(block_height*2);
			levelNumber.setHeight(block_height*2);
			currentScore.setHeight(block_height*2);
			timeRemaining.setHeight(block_height*2);
			btnquit.setHeight((int) (block_height*1.5));
			
			userHighScore.setTextColor(Color.BLACK);
			onlineHighScore.setTextColor(Color.BLACK);
			levelNumber.setTextColor(Color.BLACK);
			currentScore.setTextColor(Color.BLACK);
			timeRemaining.setTextColor(Color.BLACK);
						
			readandLoadscores();
			
			Integer yourhighscore =  NgageMainActivity.user.scores.get(NgageMainActivity.user.current_level);
			if(yourhighscore == null)
				userHighScore.setText("Your High Score : You haven't played this level yet");
			else
			userHighScore.setText("Your High Score : " + yourhighscore);

			onlineHighScore.setText(online_high_score);
			
			levelNumber.setText("Level "+NgageMainActivity.user.current_level);
			currentScore.setText("Your score : "+NgageMainActivity.user.current_score);
			timeRemaining.setText("Time Remaining :");
			btnquit.setOnClickListener(this);
			
			mytimer = new Mycountdowntimer(thisLevel.time_limit*1000, 1000);
			mytimer.start();
			startService(i);			
		}
		else
		{
			Toast.makeText(this, "Unable to fetch level data", Toast.LENGTH_LONG).show();
//			wl.release();
			finish();
		}
	}
	
	public void finishhandler()
	{
		mytimer.cancel();
		mp_finish.setVolume(500, 500);
		mp_finish.start();
//		wl.release();
		CopyOfCollectData.removeListener(this);
		fg.execute();
	}
	
	private void readandLoadscores() {
		// TODO Auto-generated method stub
		try {

				 FileInputStream fis = openFileInput("scores.dat");
				 ObjectInputStream ois = new ObjectInputStream(fis);
				 if(ois != null)
				 {
					NgageMainActivity.user.scores =  (HashMap<Integer, Integer>) ois.readObject();
				 }
				 fis.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG);
			e.printStackTrace();
		}
	}

	private void writeandSave() {
		// TODO Auto-generated method stub
		try {

				 FileOutputStream fis = openFileOutput("scores.dat",Context.MODE_PRIVATE);
				 ObjectOutputStream ois = new ObjectOutputStream(fis);
				 if(ois != null)
				 {
					ois.writeObject(NgageMainActivity.user.scores);
				 }
				 fis.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG);
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		mytimer.cancel();
//		wl.release();
	}
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	public class Mycountdowntimer extends CountDownTimer {

		public Mycountdowntimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			finishhandler();
		}

		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub
			Bundle data = new Bundle();
			data.putString(TimeRemaining,String.valueOf(millisUntilFinished / 1000));
			Message m = new Message();
			m.setData(data);
			m.what = UpdateTimer;
			handler.sendMessage(m);
		}
	}
	
	public class FinishGame extends AsyncTask<Void,Void,Void>
	{
		ProgressDialog pd;
		
		public FinishGame(Context context)
		{
			pd = new ProgressDialog(context);
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if(NgageMainActivity.user !=null && thisLevel != null)
			{
				if(NgageMainActivity.user.current_score > thisLevel.min_score_1 && NgageMainActivity.user.current_score < thisLevel.min_score_2)
					NgageMainActivity.user.booster +=1;
				else if (NgageMainActivity.user.current_score > thisLevel.min_score_2 && NgageMainActivity.user.current_score < thisLevel.min_score_3)
					NgageMainActivity.user.booster +=2;
				else if(NgageMainActivity.user.current_score > thisLevel.min_score_3)
					NgageMainActivity.user.booster +=3;
			}
			if(KeyValueAPI.isServerAvailable() && NgageMainActivity.user!=null)
			{
				String onlinehs = KeyValueAPI.get("Prachi_Pratik", "neu12345", "level_" + NgageMainActivity.user.current_level + "_hs");
				if (onlinehs.equals(""))
				{
					Log.d(TAG + "Username", NgageMainActivity.user.username);
					KeyValueAPI.put("Prachi_Pratik", "neu12345", "level_" + NgageMainActivity.user.current_level + "_hs",NgageMainActivity.user.username + "," + String.valueOf(NgageMainActivity.user.current_score));
				}
				else if(! onlinehs.contains("ERROR"))
				{
					String[] high = onlinehs.split(",");
					if(high.length==2)
					{
						int sc = Integer.parseInt(high[1]);
						if(NgageMainActivity.user.current_score > sc)
						{
							Log.d(TAG + "Username", NgageMainActivity.user.username);
							KeyValueAPI.put("Prachi_Pratik", "neu12345", "level_" + NgageMainActivity.user.current_level + "_hs",NgageMainActivity.user.username + "," + String.valueOf(NgageMainActivity.user.current_score));
						}
					}
					else
					{
//						Bundle data = new Bundle();
//						data.putString("message","Incorrect data on server.");
//						Message msg = new Message();
//						msg.what = showToast;
//						msg.setData(data);
//						handler.sendMessage(msg);
						//Toast.makeText(getApplicationContext(), "Incorrect data on server.", Toast.LENGTH_LONG).show();
					}
				}
			}
			else
			{
//				Bundle data = new Bundle();
//				data.putString("message","No Internet Connection.Unable to update score online!!");
//				Message msg = new Message();
//				msg.what = showToast;
//				msg.setData(data);
//				handler.sendMessage(msg);
				//Toast.makeText(getApplicationContext(), "No Internet Connection.Unable to update score online!!", Toast.LENGTH_LONG).show();
			}
			
			//updating local data score for this level
			if(NgageMainActivity.user.scores.containsKey(NgageMainActivity.user.current_level))
			{
				if(NgageMainActivity.user.scores.get(NgageMainActivity.user.current_level) < NgageMainActivity.user.current_score)
				{
					NgageMainActivity.user.scores.remove(NgageMainActivity.user.current_level);
					NgageMainActivity.user.scores.put(NgageMainActivity.user.current_level, NgageMainActivity.user.current_score);
					writeandSave();
				}
			}
			else
			{
				NgageMainActivity.user.scores.put(NgageMainActivity.user.current_level, NgageMainActivity.user.current_score);
				writeandSave();
			}
			
	
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pd.dismiss();
//			wl.release();
			finish();	
			Intent i1 = new Intent(getApplicationContext(), FinishNgageGame.class);
			i1.putExtra("CHALLENGE", challengeGame);
			startActivity(i1); 
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd.setMessage("Processing...");
			pd.setIndeterminate(true);
	        pd.show();
		}
		
	}
	
	public class LoadData extends AsyncTask<Void,Void,Void>
	{
		ProgressDialog pd;
		
		public LoadData(Context context)
		{
			pd = new ProgressDialog(context);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			if(KeyValueAPI.isServerAvailable())
			{
				String s = KeyValueAPI.get("Prachi_Pratik", "neu12345", "level_" + NgageMainActivity.user.current_level + "_hs");
				if(s.equals(""))
					online_high_score = "Online High Score : Nobody Played this level yet.";
				else if(!s.contains("ERROR"))
					online_high_score = "Online High Score : " + s;
				else 
					online_high_score = "Online High Score : Error fetching data";
			}
			else
					online_high_score = "Online High Score : Internet Unavailable";
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pd.dismiss();
			initializeGame();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd.setMessage("Processing...");
			pd.setIndeterminate(true);
	        pd.show();
		}
		
	}


	public void notifyAccel(Sensor sensor, long time, AccelerationData datum) {
		// TODO Auto-generated method stub
//		onlineHighScore.setText("These are the values that you need, for sensor id="
//				+ sensor._ID + ", X=" + datum._X + ", Y=" + datum._Y + ", Z="
//				+ datum._Z);
		Log.i("Wocket", "These are the values that you need, for sensor id="
				+ sensor._ID + ", X=" + datum._X + ", Y=" + datum._Y + ", Z="
			+ datum._Z);
		Logger.log(sensor._ID, datum._X + "," + datum._Y + "," + datum._Z);
		int tempscore = 0;
		if(sensor._ID == 0)
		{
			if(isValidX(datum._X))
				tempscore++;
			if(isValidY(datum._Y))
				tempscore++;
			if(isValidZ(datum._Z))
				tempscore++;
		}
		else if(sensor._ID==1)
		{
			if(isValidX2(datum._X))
				tempscore++;
			if(isValidY2(datum._Y))
				tempscore++;
			if(isValidZ2(datum._Z))
				tempscore++;	
		}
		
		if(tempscore==3)
		{
			NgageMainActivity.user.current_score+=NgageMainActivity.user.booster;
			currentScore.setText("Your score : "+NgageMainActivity.user.current_score);
			mp.setVolume(300,300);
			mp.start();
		}
	}

	private boolean isValidZ(short _Z) {
		// TODO Auto-generated method stub
		if(thisLevel.checkz == false)
			return true;
		else if(sensor1z == true && _Z >= thisLevel.min_displacement.z - thisLevel.threshold.z && _Z <= thisLevel.min_displacement.z + thisLevel.threshold.z)
		{
			sensor1z = false;
			return true;
		}
		else if(sensor1z == false && _Z >= thisLevel.max_displacement.z - thisLevel.threshold.z && _Z <= thisLevel.max_displacement.z + thisLevel.threshold.z)
		{
			sensor1z = true;
			return false;
		}
		else return false;
	}
	
	private boolean isValidZ2(short _Z) {
		// TODO Auto-generated method stub
		if(thisLevel.checkz == false)
			return true;
		else if(sensor2z == true && _Z >= thisLevel.min_displacement.z - thisLevel.threshold.z && _Z <= thisLevel.min_displacement.z + thisLevel.threshold.z)
		{
			sensor2z = false;
			return true;
		}
		else if(sensor2z == false && _Z >= thisLevel.max_displacement.z - thisLevel.threshold.z && _Z <= thisLevel.max_displacement.z + thisLevel.threshold.z)
		{
			sensor2z = true;
			return false;
		}
		else return false;
	}

	private boolean isValidY(short _Y) {
		// TODO Auto-generated method stub
		if(thisLevel.checky == false)
			return true;
		else if(sensor1y == true && _Y >= thisLevel.min_displacement.y - thisLevel.threshold.y && _Y <= thisLevel.min_displacement.y + thisLevel.threshold.y)
		{
			sensor1y = false;
			return true;
		}
		else if(sensor1y == false && _Y >= thisLevel.max_displacement.y - thisLevel.threshold.y && _Y <= thisLevel.max_displacement.y + thisLevel.threshold.y)
		{
			sensor1y = true;
			return false;
		}
		else return false;
	}
	
	private boolean isValidY2(short _Y) {
		// TODO Auto-generated method stub
		if(thisLevel.checky == false)
			return true;
		else if(sensor2y == true && _Y >= thisLevel.min_displacement.y - thisLevel.threshold.y && _Y <= thisLevel.min_displacement.y + thisLevel.threshold.y)
		{
			sensor2y = false;
			return true;
		}
		else if(sensor2y == false && _Y >= thisLevel.max_displacement.y - thisLevel.threshold.y && _Y <= thisLevel.max_displacement.y + thisLevel.threshold.y)
		{
			sensor2y = true;
			return false;
		}
		else return false;
	}

	private boolean isValidX(short _X) {
		// TODO Auto-generated method stub
		if(thisLevel.checkx == false)
			return true;
		else if(sensor1x == true && _X >= thisLevel.min_displacement.x - thisLevel.threshold.x && _X <= thisLevel.min_displacement.x + thisLevel.threshold.x)
		{
			sensor1x = false;
			return true;
		}
		else if(sensor1x == false && _X >= thisLevel.max_displacement.x - thisLevel.threshold.x && _X <= thisLevel.max_displacement.x + thisLevel.threshold.x)
		{
			sensor1x = true;
			return false;
		}
		else return false;
	}
	private boolean isValidX2(short _X) {
		// TODO Auto-generated method stub
		if(thisLevel.checkx == false)
			return true;
		else if(sensor2x == true && _X >= thisLevel.min_displacement.x - thisLevel.threshold.x && _X <= thisLevel.min_displacement.x + thisLevel.threshold.x)
		{
			sensor2x = false;
			return true;
		}
		else if(sensor2x == false && _X >= thisLevel.max_displacement.x - thisLevel.threshold.x && _X <= thisLevel.max_displacement.x + thisLevel.threshold.x)
		{
			sensor2x = true;
			return false;
		}
		else return false;
	}


	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.btnquit:
			mytimer.cancel();
			CopyOfCollectData.removeListener(this);
			//stopService(i);
			Intent im = new Intent(this, LevelMenu.class);
			startActivity(im);
			break;
		}
	}

}
