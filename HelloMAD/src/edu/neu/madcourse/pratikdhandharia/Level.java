package edu.neu.madcourse.pratikdhandharia;

import java.io.Serializable;

public class Level implements Serializable{
	public int level_number;
	public Acceleration threshold;
	public Acceleration max_displacement;
	public Acceleration min_displacement;
	public long time_limit;
	public int min_score_1;
	public int min_score_2;
	public int min_score_3;
	public boolean checkx,checky,checkz;
	
	public Level()
	{
		threshold = new Acceleration();
		max_displacement = new Acceleration();
		min_displacement = new Acceleration();
		checkx = false;
		checky=false;
		checkz=false;
	}
	
	public Level(int ln,Acceleration t,Acceleration mxd, Acceleration mnd, long tl,int ms1,int ms2,int ms3,boolean cx,boolean cy,boolean cz)
	{
		level_number = ln;
		threshold = t;
		max_displacement = mxd;
		min_displacement = mnd;
		time_limit = tl;
		min_score_1 = ms1;
		min_score_2 = ms2;
		min_score_3 = ms3;
		checkx = cx;
		checky = cy;
		checkz = cz;
	}
	
}


