package edu.neu.madcourse.pratikdhandharia;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class PersistentBoggleView extends View{


	private static final String TAG = "PersistentBoggleView";
	private final PersistentBoggleGame persbogglegame;
	
	private float width;    // width of one tile
	private float height;   // height of one tile
	private float grid_start,box_width,box_height;
	
	public PersistentBoggleView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
		
		persbogglegame = (PersistentBoggleGame) context;
		
	    setFocusable(true);
	    setFocusableInTouchMode(true);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		// TODO Auto-generated method stub
		try{
		super.onDraw(canvas);
		width = getWidth();
		height = getHeight();
		grid_start = height/3;
		box_height = (height - grid_start)/5;
		box_width = (width/5);	
		
		Paint background = new Paint();
	      background.setColor(getResources().getColor(
	            R.color.puzzle_background));
	     canvas.drawRect(0, 0, getWidth(), (box_height * 5), background);
	     
	      Paint dark = new Paint();
	      dark.setColor(getResources().getColor(R.color.puzzle_dark));
	      
	      canvas.drawLine(0, 0, getWidth(), 0, dark);
	      canvas.drawLine(0, box_height, getWidth(), box_height, dark);
	      canvas.drawLine(0,(box_height*2), getWidth(),(box_height * 2), dark);
	      canvas.drawLine(0,(box_height*3), getWidth(),(box_height * 3), dark);
	      canvas.drawLine(0,(box_height*4), getWidth(),(box_height * 4), dark);
	      canvas.drawLine(0,(box_height*5), getWidth(),(box_height * 5), dark);
	      
	      canvas.drawLine(box_width, 0, box_width, (box_height * 5), dark);
	      canvas.drawLine(box_width * 2, 0, box_width * 2, (box_height * 5), dark);
	      canvas.drawLine(box_width * 3, 0, box_width * 3, (box_height * 5), dark);
	      canvas.drawLine(box_width * 4, 0, box_width * 4, (box_height * 5), dark);
	      
	      Paint foreground = new Paint(Paint.ANTI_ALIAS_FLAG);
	      foreground.setColor(getResources().getColor(
	            R.color.puzzle_foreground));
	      foreground.setStyle(Style.FILL);
	      foreground.setTextSize(box_height * 0.75f);
	      foreground.setTextScaleX(box_width / box_height);
	      foreground.setTextAlign(Paint.Align.CENTER);

	      // Draw the number in the center of the tile
	      FontMetrics fm = foreground.getFontMetrics();
	      // Centering in X: use alignment (and X at midpoint)
	      float x = box_width / 2;
	      // Centering in Y: measure ascent/descent first
	      float y = box_height / 2 - (fm.ascent + fm.descent) / 2;
	      int at = 0;
	      for (int i = 0; i < 5; i++) {
	          for (int j = 0; j < 5; j++) {
	             canvas.drawText(this.persbogglegame.getStringAt(at), j * box_width + x, i * box_height + y, foreground);
	             at = at +1;
	          }
	       }
		}
		catch(Exception e)
		{
			Log.d(TAG,"Stacktrace: " + e.getStackTrace());
		}
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		// TODO Auto-generated method stub
		if(event.getAction() == MotionEvent.ACTION_DOWN)
		{
			int selX, selY,index;
			boolean isvalidclickcheck;
			selX = (int) Math.floor(event.getX()/box_width);
			selY = (int) Math.floor(event.getY()/box_height);
			
			index = selX + (selY * 5);
			isvalidclickcheck = isvalidclick(index,selX, selY);
			if (isvalidclickcheck == true)
			{
			persbogglegame.mp_letter.start();
			persbogglegame.currentWord += persbogglegame.getStringAt(index);
			persbogglegame.txtCurrentWord.setText(persbogglegame.currentWord);
			persbogglegame.persbogglewordsindex.add(index);
			}
	//	Log.d("Mouse-Event","Mouse-x: " + String.valueOf(Math.floor(event.getX()/box_width)) + "Mouse-y: " + String.valueOf(Math.floor(event.getY()/box_height)));
		}
		return super.onTouchEvent(event);
		
	}
	
	public boolean isvalidclick(int i, int selX, int selY)
	{
		boolean isRepeat,isWithinRange,isnear;
		isnear = false;
		isRepeat = persbogglegame.isalreadyclicked(i);
		isWithinRange = i>=0 && i<25;
		int previousClicked = persbogglegame.getlastclicked();
		int previousX, previousY;
		if (previousClicked == -1) 
			isnear = true;
		else
		{
			
			previousY = (int) Math.floor(previousClicked/5);
			previousX = previousClicked - (previousY * 5);
			if (Math.abs(selX-previousX) <= 1 && Math.abs(selY - previousY) <= 1)
			{
				isnear = true;
			}
		}
		
		if (isWithinRange && !isRepeat && isnear)
		return true;
		return false;
	}
	
	
	

}
