package edu.neu.madcourse.pratikdhandharia;

import java.util.ArrayList;
import java.util.Map;

import android.app.Activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class Ngage extends Activity implements OnClickListener{
	
	private static final String USERNAME = "username";
	private static final String PASSWORD = "password";
	private static final String SCORES = "scores";
	private static final String TOTALCHALLENGES = "totalchallenges";
	private static final String TOTALWON = "totalwon";
	private static final String BOOSTER = "booster";
	private static final String CHALLENGES = "challenges";
	private static final String CHALLENGED = "challenged";

	private Button btnhighscore,btnplay,btnexit,btnstats,btnlearnwockets;
	
	public static Levels listOfLevels;
	public static int currentLevel = 0;
	public static int currentscore = 0;
	public static Boolean notification = true;
	public static Boolean dataNotCleared = false;
	public AlarmManager am;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ngage_activity);
		if(NgageMainActivity.user != null)
			setTitle(NgageMainActivity.user.username);
		updateListOfLevels();
		
		Display display = getWindowManager().getDefaultDisplay();
		int width = (int)( display.getWidth() *0.7);
		int height = (int) (display.getHeight()/10);
		//restoreData();
		
		btnlearnwockets = (Button) findViewById(R.id.btnlearnwockets);
		btnlearnwockets.setOnClickListener(this);
		btnlearnwockets.setBackgroundResource(R.drawable.button);
		btnlearnwockets.setTextColor(Color.WHITE);
		btnlearnwockets.setWidth(width);
		btnlearnwockets.setHeight(height);
		
		btnhighscore = (Button) findViewById(R.id.btnhighscore);
		btnhighscore.setOnClickListener(this);
		btnhighscore.setBackgroundResource(R.drawable.button);
		btnhighscore.setTextColor(Color.WHITE);
		btnhighscore.setWidth(width);
		btnhighscore.setHeight(height);
		
		btnstats = (Button) findViewById(R.id.btnchallengestats);
		btnstats.setOnClickListener(this);
		btnstats.setBackgroundResource(R.drawable.button);
		btnstats.setTextColor(Color.WHITE);
		btnstats.setWidth(width);
		btnstats.setHeight(height);
		
		btnplay = (Button) findViewById(R.id.btnPlay);
		btnplay.setOnClickListener(this);
		btnplay.setBackgroundResource(R.drawable.button);
		btnplay.setTextColor(Color.WHITE);
		btnplay.setWidth(width);
		btnplay.setHeight(height);
		
		btnexit = (Button) findViewById(R.id.btnngageexit);
		btnexit.setOnClickListener(this);
		btnexit.setBackgroundResource(R.drawable.button);
		btnexit.setTextColor(Color.WHITE);
		btnexit.setWidth(width);
		btnexit.setHeight(height);
		
		am = (AlarmManager) getSystemService(ALARM_SERVICE);
		setRepeatingAlarm();
	}

	private void setRepeatingAlarm() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, TimeAlarm.class);
		  PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
		    intent, PendingIntent.FLAG_CANCEL_CURRENT);
		  am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
		    (20 * 1000), pendingIntent);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//saveData();
	}
	
	public void saveData()
	{
		getPreferences(MODE_PRIVATE).edit()
		.putString(USERNAME, NgageMainActivity.user.username)
		.putString(PASSWORD, NgageMainActivity.user.password)
		.putInt(TOTALCHALLENGES, NgageMainActivity.user.totalChallenges)
		.putInt(TOTALWON,NgageMainActivity.user.totalWon)
		.putInt(BOOSTER, NgageMainActivity.user.booster)
		.commit();
		
		SharedPreferences sp = getSharedPreferences(SCORES, Context.MODE_PRIVATE);
		SharedPreferences.Editor sp_edit = sp.edit();
		
		for(Integer i : NgageMainActivity.user.scores.keySet())
		{
			sp_edit.putInt(String.valueOf(i), NgageMainActivity.user.scores.get(i));
		}
		sp_edit.commit();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//restoreData();
	}
	
	public void restoreData()
	{
		SharedPreferences sp = getSharedPreferences(SCORES, Context.MODE_PRIVATE);
		Map<String,?> hm = sp.getAll();
		if(hm != null)
		{
		Log.d(SCORES, "" + hm.size());
		Toast.makeText(this, "" + hm.size(), Toast.LENGTH_LONG);
		for(String s : hm.keySet())
		{
			Object o = hm.get(s);
			NgageMainActivity.user.scores.put(Integer.parseInt(s), (Integer) o);
		}
		}
		
		NgageMainActivity.user.username = getPreferences(MODE_PRIVATE).getString(USERNAME, "");
		NgageMainActivity.user.password = getPreferences(MODE_PRIVATE).getString(PASSWORD, "");
		NgageMainActivity.user.totalChallenges = getPreferences(MODE_PRIVATE).getInt(TOTALCHALLENGES, 0);
		NgageMainActivity.user.totalWon = getPreferences(MODE_PRIVATE).getInt(TOTALWON, 0);
		NgageMainActivity.user.booster = getPreferences(MODE_PRIVATE).getInt(BOOSTER, 1);
	}	

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.btnPlay:
			Intent i = new Intent(this, LevelMenu.class);
			startActivity(i);
			break;
		case R.id.btnhighscore:
			Intent ihs = new Intent(this, HighScoreNgage.class);
			startActivity(ihs);
			break;
		case R.id.btnchallengestats:
			Intent im = new Intent(this, ChallengeMain.class);
			startActivity(im);
			break;
		case R.id.btnngageexit:
			//saveData();
			finish();
			break;
		case R.id.btnlearnwockets:
			Intent iw = new Intent(this, ShowWockets.class);
			startActivity(iw);
			break;
		default: break;
		}
		
	}
	
	private void updateListOfLevels() {
		// TODO Auto-generated method stub
		// hand up down
		Level l1 = new Level(1, new Acceleration(50, 50, 50), new Acceleration(-1, -1, 600), new Acceleration(-1, -1, 100), 60, 90, 100, 110,false,false,true);
		Level l2 = new Level(2, new Acceleration(50, 50, 50), new Acceleration(-1, -1, 600), new Acceleration(-1, -1, 100), 50, 80, 90, 100,false,false,true);
		Level l3 = new Level(3, new Acceleration(50, 50, 50), new Acceleration(-1, -1, 600), new Acceleration(-1, -1, 100), 40, 70, 80, 90,false,false,true);
		Level l4 = new Level(4, new Acceleration(50, 50, 50), new Acceleration(-1, -1, 600), new Acceleration(-1, -1, 100), 30, 60, 70, 80,false,false,true);
		Level l5 = new Level(5, new Acceleration(50, 50, 50), new Acceleration(-1, -1, 600), new Acceleration(-1, -1, 100), 20, 50, 60, 70,false,false,true);
		
		// hand front back
		Level l6 = new Level(6, new Acceleration(50, 50, 50), new Acceleration(800,-1,-1), new Acceleration(300, -1,-1), 60, 90, 100, 110,true,false,false);
		Level l7 = new Level(7, new Acceleration(50, 50, 50), new Acceleration(800,-1,-1), new Acceleration(300, -1,-1), 50, 80, 90, 100,true,false,false);
		Level l8 = new Level(8, new Acceleration(50, 50, 50), new Acceleration(800,-1,-1), new Acceleration(300, -1,-1), 40, 70, 80, 90,true,false,false);
		Level l9 = new Level(9, new Acceleration(50, 50, 50), new Acceleration(800,-1,-1), new Acceleration(300, -1,-1), 30, 60, 70, 80,true,false,false);
		Level l10 = new Level(10, new Acceleration(50, 50, 50), new Acceleration(800,-1,-1), new Acceleration(300, -1,-1), 20, 50, 60, 70,true,false,false);
		
		//legs running

		Level l11 = new Level(11, new Acceleration(50, 50, 50), new Acceleration(-1,700,-1), new Acceleration(-1, 0, -1), 60, 120,130,140,false,true,false);
		Level l12 = new Level(12, new Acceleration(50, 50, 50), new Acceleration(-1,700,-1), new Acceleration(-1, 0, -1), 50, 110,120,130,false,true,false);
		Level l13 = new Level(13, new Acceleration(50, 50, 50), new Acceleration(-1,700,-1), new Acceleration(-1, 0, -1), 40, 100,110,120,false,true,false);
		Level l14 = new Level(14, new Acceleration(50, 50, 50), new Acceleration(-1,700,-1), new Acceleration(-1, 0, -1), 30, 90,100,110,false,true,false);
		Level l15 = new Level(15, new Acceleration(50, 50, 50), new Acceleration(-1,700,-1), new Acceleration(-1, 0, -1), 20, 80,90,100,false,true,false);
		
		//hand running
		Level l16 = new Level(16, new Acceleration(50, 50, 50), new Acceleration(550,-1,-1), new Acceleration(100, -1, -1), 60, 90, 100, 110, true,false,false);
		Level l17 = new Level(17, new Acceleration(50, 50, 50), new Acceleration(550,-1,-1), new Acceleration(100, -1, -1), 50, 80, 90, 100, true,false,false);
		Level l18 = new Level(18, new Acceleration(50, 50, 50), new Acceleration(550,-1,-1), new Acceleration(100, -1, -1), 40, 70, 80, 90, true,false,false);
		Level l19 = new Level(19, new Acceleration(50, 50, 50), new Acceleration(550,-1,-1), new Acceleration(100, -1, -1), 30, 60, 70, 80, true,false,false);
		Level l20 = new Level(20, new Acceleration(50, 50, 50), new Acceleration(550,-1,-1), new Acceleration(100, -1, -1), 20, 50, 60, 70, true,false,false);
		
		ArrayList<Level> levels = new ArrayList<Level>();
		levels.add(l1);
		levels.add(l2);
		levels.add(l3);
		levels.add(l4);
		levels.add(l5);
		levels.add(l6);
		levels.add(l7);
		levels.add(l8);
		levels.add(l9);
		levels.add(l10);
		levels.add(l11);
		levels.add(l12);
		levels.add(l13);
		levels.add(l14);
		levels.add(l15);
		levels.add(l16);
		levels.add(l17);
		levels.add(l18);
		levels.add(l19);
		levels.add(l20);
		listOfLevels = new Levels(levels);
	}

}
