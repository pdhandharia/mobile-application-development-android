package edu.neu.madcourse.pratikdhandharia;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class Rules extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.rules);
		
		TextView txtrules = (TextView) findViewById(R.id.txtrules);
		txtrules.setText(getIntent().getStringExtra("rules"));
		setTitle("Rules!!");
	}

}
