package edu.neu.madcourse.pratikdhandharia;

import java.io.Serializable;

public class Acceleration implements Serializable{
	public int x;
	public int y;
	public int z;
	
	public Acceleration(int x, int y, int z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Acceleration(){
		x = 0;
		y = 0;
		z = 0;
	}
}
