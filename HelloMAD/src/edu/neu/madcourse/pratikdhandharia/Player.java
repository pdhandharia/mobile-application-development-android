package edu.neu.madcourse.pratikdhandharia;

public class Player {
	public String playerName;
	public int score;
	public Long last_move;
	public Player opponent;
	
	public Player(String playerName, long lastmove) {
		super();
		this.playerName = playerName;
		this.last_move = lastmove;
		this.score = 0;
	}
	
	
	
}
