package edu.neu.madcourse.pratikdhandharia;

import java.io.Serializable;
import java.util.ArrayList;

public class Levels implements Serializable {
	public ArrayList<Level> levels;
	
	public Levels()
	{
		levels = new ArrayList<Level>();
	}
	
	public Levels (ArrayList<Level> _levels)
	{
		levels = _levels;
	}
}
