package edu.neu.madcourse.pratikdhandharia;

import com.everyfit.wockets.data.AccelerationData;
import com.everyfit.wockets.sensors.Sensor;

public interface DataServiceInterface {

	public void notifyAccel(Sensor sensor, long time, AccelerationData datum);
}
