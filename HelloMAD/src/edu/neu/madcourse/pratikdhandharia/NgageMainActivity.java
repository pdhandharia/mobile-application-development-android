package edu.neu.madcourse.pratikdhandharia;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.widget.Toast;

public class NgageMainActivity extends Activity{
	
	public static final String NEWUSER = "newuser";
	public static final String NGAGEUSER = "ngageuser";
	public static final String NEWUSERPASSWORD = "newuserpassword";
	public static final String NGAGEUSERPASSWORD = "ngageuserpassword";
	final Context context = this;
	public static User user;
	public static int width;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	    user = new User();
	    readandLoadscores();
        Display display = getWindowManager().getDefaultDisplay();
        width = (int) ((display.getWidth()/2)*0.8);
        
        if (user.username.equals("") && getSharedPreferences(NEWUSER, Activity.MODE_PRIVATE).contains(NGAGEUSER))
//				&& user.password.equals("") && getSharedPreferences(NEWUSERPASSWORD, Activity.MODE_PRIVATE).contains(NGAGEUSERPASSWORD))
		{
			user.username = getSharedPreferences(NEWUSER, Activity.MODE_PRIVATE)
					.getString(NGAGEUSER,"");
//			user.password = getSharedPreferences(NEWUSERPASSWORD, Activity.MODE_PRIVATE)
//					.getString(NGAGEUSERPASSWORD,"");
			
		}
	    if (user.username.equals(""))
//	    		&& user.password.equals(""))
	    {
	    	Intent i3 = new Intent(context,SignUp.class);
			startActivity(i3);
	    }
	    else 
	    {
	    	Intent i3 = new Intent(context,Ngage.class);
			startActivity(i3);
	    }
	}
	private void readandLoadscores() {
		// TODO Auto-generated method stub
		try {

				 FileInputStream fis = openFileInput("scores.dat");
				 ObjectInputStream ois = new ObjectInputStream(fis);
				 if(ois != null)
				 {
					NgageMainActivity.user.scores =  (HashMap<Integer, Integer>) ois.readObject();
				 }
				 fis.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG);
			e.printStackTrace();
		}
	}

}
