package edu.neu.madcourse.pratikdhandharia;

import java.util.ArrayList;
import java.util.HashMap;

import edu.neu.mobileclass.apis.KeyValueAPI;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TableRow.LayoutParams;
import android.widget.TextView;

public class HighScoreNgage extends Activity{
	
	TableLayout tblayout;
	TableRow tbrow;
	TextView level, yourHS, onlineHS;
	ProgressDialog mProgressDialog;
	HashMap<Integer, String> onlineScores;
	Button btnback;
	
	
	class downloadScores extends AsyncTask<Void, Void, Void>{

		@Override
	protected Void doInBackground(Void... params) {
		// TODO Auto-generated method stub
			onlineScores = new HashMap<Integer, String>();
			for (int i=1; i<=Ngage.listOfLevels.levels.size();i++)
			{
				if (KeyValueAPI.isServerAvailable())
				{
					String score = KeyValueAPI.get("Prachi_Pratik", "neu12345", "level_"+i+"_hs");
					if (score.equals(""))
						onlineScores.put(i, "");
					else
					{
						onlineScores.put(i, score.trim());
					}
				}
			}
			return null;
	}
		
		@Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        
			mProgressDialog.setMessage("Please wait! Scores loading...");
			mProgressDialog.setIndeterminate(true);
	        mProgressDialog.show();
	    }

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			mProgressDialog.dismiss();
			populateTable();
		}
}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.highscorengage);
		
		mProgressDialog = new ProgressDialog(this);
		downloadScores ds = new downloadScores();
		ds.execute();
		
		tblayout = (TableLayout) findViewById(R.id.highscoretable);
		tbrow = new TableRow(this);
		level = new TextView(this);
		yourHS = new TextView(this);
		onlineHS = new TextView(this);
		

		
		tbrow.setLayoutParams(new LayoutParams(
				LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
		
		level.setId(10);
		level.setPadding(5, 5, 5, 5);
		level.setText("Level");
		yourHS.setId(11);
		yourHS.setPadding(5, 5, 5, 5);
		yourHS.setText("Your High Score");
		onlineHS.setId(12);
		onlineHS.setPadding(5, 5, 5, 5);
		onlineHS.setText("Online High Score");
		
		tbrow.addView(level);
		tbrow.addView(yourHS);
		tbrow.addView(onlineHS);
		
		tblayout.addView(tbrow);
		
		Log.d("HIGH SCORE", Ngage.listOfLevels.levels.size()+"");
		
	}

	public void populateTable() {
		// TODO Auto-generated method stub
		for (int i=1; i<=Ngage.listOfLevels.levels.size();i++)
		{
			tbrow = new TableRow(this);
			level = new TextView(this);
			yourHS = new TextView(this);
			onlineHS = new TextView(this);
			
			tbrow.setLayoutParams(new LayoutParams(
					LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT));
			
			level.setId(10);
			level.setPadding(5, 5, 5, 5);
			level.setText(Integer.toString(i));
			yourHS.setId(11);
			yourHS.setPadding(5, 5, 5, 5);
			if (NgageMainActivity.user.scores.get(i) == null)
				yourHS.setText("-");
			else
				yourHS.setText(NgageMainActivity.user.scores.get(i).toString());
			onlineHS.setId(12);
			onlineHS.setPadding(5, 5, 5, 5);
			
			if (onlineScores.get(i) == null || onlineScores.get(i).equals(""))
				onlineHS.setText("-");
			else
				onlineHS.setText(onlineScores.get(i).toString());
			
			tbrow.addView(level);
			tbrow.addView(yourHS);
			tbrow.addView(onlineHS);
			tblayout.addView(tbrow);
			
		}
		
		tbrow = new TableRow(this);
		btnback = new Button(this);
		btnback.setText("Back");
		btnback.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		//btnback.setLayoutParams(new LayoutParams(this, ))
		TableRow.LayoutParams lp = new LayoutParams();
		lp.span = 3;
		btnback.setLayoutParams(lp);
		tbrow.addView(btnback);
		tblayout.addView(tbrow);
	}
	
	
}
