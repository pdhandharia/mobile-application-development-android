package edu.neu.madcourse.pratikdhandharia;

import edu.neu.mobileclass.apis.KeyValueAPI;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ChallengeRequest extends Activity implements OnClickListener{

	private Button btnaccept, btndecline;
	private TextView txtmsg;
	private int block_width;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.challenge_request);
		Ngage.notification = false;
		String opponent = getIntent().getStringExtra("OPPONENT");
		Display display = getWindowManager().getDefaultDisplay();
		block_width = display.getWidth() / 2 ;
		
		btnaccept = (Button) findViewById(R.id.btnaccept);
		btnaccept.setOnClickListener(this);
		btnaccept.setWidth((int) (block_width *0.8));
		btndecline = (Button) findViewById(R.id.btndecline);
		btndecline.setOnClickListener(this);
		btndecline.setWidth((int) (block_width * 0.8));
		txtmsg = (TextView) findViewById(R.id.txtmsg);
		txtmsg.setText("Do you want to accept challenge from "+opponent+"?");
		txtmsg.setWidth((int) (block_width*0.8));
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.btnaccept:
			acceptChallenge();
			break;
		case R.id.btndecline:
			if(KeyValueAPI.isServerAvailable())
			{
				String opponent = KeyValueAPI.get("Prachi_Pratik", "neu12345", NgageMainActivity.user.username);
				KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", NgageMainActivity.user.username);
				KeyValueAPI.put("Prachi_Pratik", "neu12345", opponent+"_challengeScore", Integer.toString(-1));
				Ngage.notification = true;
			}
			Intent i = new Intent(this, Ngage.class);
			startActivity(i);
			break;
		}
	}

	private void acceptChallenge() {
		// TODO Auto-generated method stub
		Opponent opp = new Opponent();
		opp.username = getIntent().getStringExtra("OPPONENT");
		if (KeyValueAPI.isServerAvailable())
		{
			String levelstr = KeyValueAPI.get("Prachi_Pratik", "neu12345", NgageMainActivity.user.username+"_level"); 
			if (! levelstr.equals(""))
			{
				opp.level = Integer.parseInt(levelstr.trim()); 
			}
			else
			{
				Toast.makeText(this, "No level information on server!!", Toast.LENGTH_SHORT).show();
				finish();
			}
			String scorestr = KeyValueAPI.get("Prachi_Pratik", "neu12345", NgageMainActivity.user.username+"_score"); 
			if (! scorestr.equals(""))
			{
				opp.score = Integer.parseInt(scorestr); 
			}
			else
			{
				Toast.makeText(this, "No score information on server!!", Toast.LENGTH_SHORT).show();
				finish();
			}
			if (NgageMainActivity.user.addChallenges(opp))
			{
				Intent i = new Intent(this, NewNGageGame.class);
				i.putExtra("CHALLENGE", true);
				startActivity(i);
			}
			else
			{
				Toast.makeText(this, "Cannot accept challenge from more than 1 user!!", Toast.LENGTH_SHORT).show(); 
			}
		}
		else
		{
			Toast.makeText(this, "No Internet Connection!!", Toast.LENGTH_SHORT).show();
			finish();
		}
	}
}
