package edu.neu.madcourse.pratikdhandharia;

import edu.neu.mobileclass.apis.KeyValueAPI;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Register extends Activity implements OnClickListener{
	public static final String USER_KEY = "user";
	public static final String USERNAME = "username";
	Button btnregister;
	TextView textusername;
	EditText editusername;
	int height, width;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register);
		
		btnregister = (Button) findViewById(R.id.btnregister);
		textusername = (TextView) findViewById(R.id.txtusername);
		editusername = (EditText) findViewById(R.id.editusername);
		
		Display display = getWindowManager().getDefaultDisplay();
		height = display.getHeight();
		width = display.getWidth();

		textusername.setWidth(width / 2);
		editusername.setWidth(width / 2);
		
		btnregister.setOnClickListener(this);
	}
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnregister:
			//clearKeys();
			btnregisterhandler();
			break;
			}
		}
	private void btnregisterhandler() {
		// TODO Auto-generated method stub
		String un = editusername.getEditableText().toString();
		// TODO Auto-generated method stub
		if(!un.equals(""))
		{
			if (KeyValueAPI.isServerAvailable())
			{
				if (KeyValueAPI.get("Prachi_Pratik", "neu12345", un+"_online").equals("true"))
				{
					Toast t = Toast.makeText(this, "User Already Logged In", Toast.LENGTH_SHORT);
					t.show();
				}
				else
				{
				// adding username in shared preference
				getSharedPreferences(USER_KEY, Activity.MODE_PRIVATE).edit()
				.putString(USERNAME,editusername.getEditableText().toString())
				.commit();

				MainActivity.username = un;

				String temp = KeyValueAPI.get("Prachi_Pratik", "neu12345", "ListOfUsers");
				if(!temp.contains(un))
				{
					temp = temp + "," + un;
					KeyValueAPI.put("Prachi_Pratik","neu12345", "ListOfUsers", temp);
				}
				KeyValueAPI.put("Prachi_Pratik", "neu12345", MainActivity.username+"_online", "true");
				Intent i3 = new Intent(this,PersistentBoggle.class);
				startActivity(i3);
				}
				
			}
			else
			{
				Toast t = Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT);
				t.show();
			}
		}
	}
}