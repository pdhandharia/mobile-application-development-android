package edu.neu.madcourse.pratikdhandharia;

import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import edu.neu.mobileclass.apis.KeyValueAPI;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

public class PersistentBoggle extends Activity implements OnClickListener {
	private static final String TAG = "PersistentBoggle";
	private static final String ISLOCAL = "ISLOCAL";
	private static final int ShowDialog = 1;
	private static final int NoInternetToast = 2;
	private static final int OpponentNotRespondedToast = 3;
	private static final int ShowDialogTwoRequest = 4;
	ProgressDialog mProgressDialog;
	CountDownTimer waitTSTimer;
	public static Player player1, player2;
	public static String persboggle;
	
	public static String possible_opponent;
	static int mycounter = 0;
	static int oppcounter = 0;
	static int waitcounter = 0;
	
	final Context context = this;
	Timer mytimer;
	static PersistentBoggleGame persbogglegame;
	//this is used when I accept a challenge from other player
	TimerTask checkgame;
	
	Handler handler;
	DialogInterface.OnClickListener accept,decline;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.persistentboggle);
		setTitle(MainActivity.username);	
		Log.d(TAG, "on create persistent boggle");
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		mProgressDialog = new ProgressDialog(context);
		persbogglegame = new PersistentBoggleGame();
		accept = new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub

				// TODO: challenge accepted and now waiting for other player to create 
				// timestamp and game
				KeyValueAPI.put("Prachi_Pratik", "neu12345", possible_opponent, MainActivity.username);
				KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", player2.playerName);
				player2.playerName = possible_opponent;
				player1.opponent = player2;
				player2.opponent = player1;
				
				waitforTimeStamp();
			}
		};
		decline = new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				possible_opponent = "";
				KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", MainActivity.username);
				Log.d(TAG, "challengeRequest : request declined from other opponent");
				polling();
			}
		};
		
		
		handler = new Handler(){

			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				String possible_opponent;
				super.handleMessage(msg);
				switch(msg.what)
				{
				case ShowDialogTwoRequest:
				case ShowDialog:
					Log.d(TAG, "handler called - ShowDialog !!");
					AlertDialog.Builder bd = new AlertDialog.Builder(context);
					bd.setTitle("Challenge Request");
					possible_opponent = msg.getData().getString("opponent");
					bd.setMessage("Do you want to accept the challenge from "+possible_opponent+"?");
					bd.setPositiveButton("Accept", accept);
					bd.setNegativeButton("Decline",decline);
					AlertDialog ad = bd.create();
					ad.show();
					break;
				case NoInternetToast:
					Log.d(TAG, "no internet toast dialog");
					Toast t1 = Toast.makeText(context, "Internet connection lost.", Toast.LENGTH_SHORT);
					t1.show();
					break;
				case OpponentNotRespondedToast:
					Log.d(TAG, "opponent not responded toast dialog");
					Toast t2 = Toast.makeText(context, "Opponent lost connection or is taking too much time to respond.", 
							Toast.LENGTH_SHORT);
					t2.show();
					break;
				}
			}
			
		};

		//Button Click for Resume
		//View btnboggleresume = (View) findViewById(R.id.btnpersistentResume);
		
		//btnboggleresume.setOnClickListener(this);
		
		View btnbogglenewGame = (View) findViewById(R.id.btnpersistentnewGame);
		btnbogglenewGame.setOnClickListener(this);
		
		View btnboggleQuit = (View) findViewById(R.id.btnpersistentQuit);
		btnboggleQuit.setOnClickListener(this);
		
		View btnboggleRules = (View) findViewById(R.id.btnpersistentRules);
		btnboggleRules.setOnClickListener(this);
		
		View btnboggleAcknowledgement = (View) findViewById(R.id.btnpersistentAcknowledgement);
		btnboggleAcknowledgement.setOnClickListener(this);
		
		player1 = new Player(MainActivity.username, System.currentTimeMillis());
		player2 = new Player("", System.currentTimeMillis());
		
		polling();
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent i;
		switch(v.getId())
		{
//		case R.id.btnpersistentResume:
//			//TODO: add the PREF
//			i = new Intent(this,PersistentBoggleGame.class);
//			i.putExtra(PersistentBoggleGame.KEY,0);
//			startActivity(i);
//			break;
		case R.id.btnpersistentnewGame:
			//TODO: add the PREF
			newGameBtnHandler();
			break;
		case R.id.btnpersistentQuit:
			mytimer.cancel();
			KeyValueAPI.put("Prachi_Pratik", "neu12345", MainActivity.username+"_online", "false");
			finish();
			break;
		case R.id.btnpersistentRules:
			i = new Intent(this,Rules.class);
			i.putExtra("rules", getString(R.string.persdescRules));
			startActivity(i);
			break;
		case R.id.btnpersistentAcknowledgement:
			i = new Intent(this,Acknowledgement.class);
			i.putExtra("ack", getString(R.string.persdescAck));
			startActivity(i);
			break;
		}
	}
	
	public void polling(){
		
		mytimer = new Timer();
		mytimer.scheduleAtFixedRate(new TimerTask() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				//Log.d(TAG, "TimerTask = "+String.valueOf(KeyValueAPI.isServerAvailable()));
				if(KeyValueAPI.isServerAvailable())
				{
					String opponentusername = KeyValueAPI.get("Prachi_Pratik", "neu12345", MainActivity.username).trim();
					mycounter = 0;
					if(oppcounter >= 60)
					{
						//player2 didn't reply, no one challenged player1 and timeout
						mytimer.cancel();
						Log.d(TAG, "polling : Opponent Time out. No reply from opponent for 30 secs");
						KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", player2.playerName);
						KeyValueAPI.put("Prachi_Pratik", "neu12345", MainActivity.username+"_online", "false");
						oppcounter = 0;
						finish();
					}
					else if (opponentusername.equals(""))
					{
						//player2 didn't reply, no one challenged player1 and time still remaining
						oppcounter++;
						Log.d(TAG, "polling : Opponent Counter: "+oppcounter);
					}
					else
					{
						// either player2 accepted my challenge or player1 was challenged by someone
						mytimer.cancel();
						mycounter=0;
						oppcounter=0;
						Log.d(TAG, "polling : Key-value pair created : "+MainActivity.username+" "+
								KeyValueAPI.get("Prachi_Pratik", "neu12345", MainActivity.username).trim());
						possible_opponent = opponentusername;
						initiateGame(player2.playerName,
								opponentusername);
					}
				}
				else
				{
					mycounter++;
					Log.d(TAG, "No Internet Timer : " + mycounter);
					if (mycounter ==30)
					{
						mytimer.cancel();
						// todo: clear keys after internet is back
						Log.d(TAG, "polling : My Time out. No internet connection for 30 secs");
					
						mycounter =0;
						Message msg = new Message();
						msg.what = NoInternetToast;
						handler.sendMessage(msg);
						KeyValueAPI.put("Prachi_Pratik", "neu12345", MainActivity.username+"_online", "false");
						finish();
					}
				}
			}
		}, 0, 500);

	}
	class WaitForTimeStamp extends AsyncTask<Void, Void, Void>{
		
		Boolean isInternet = true;

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			
			while (waitcounter <= 150)
			{
				if (KeyValueAPI.isServerAvailable())
				{
					if(KeyValueAPI.get("Prachi_Pratik", "neu12345", player1.playerName+"_TS").trim().equals("") ||
							KeyValueAPI.get("Prachi_Pratik", "neu12345", player2.playerName+"_TS").trim().equals("") ||
							KeyValueAPI.get("Prachi_Pratik", "neu12345", player2.playerName+"-"+player1.playerName).trim().equals(""))
					{
						waitcounter++;
						//Log.d(TAG, "unable to find data");
					}
						else
						break;
				}
				else
				{
					isInternet = false;
					mProgressDialog.dismiss();
					Message msg = new Message();
					msg.what = NoInternetToast;
					handler.sendMessage(msg);
					//persbogglegame.startLocalGame();
					
					Intent i = new Intent(context, PersistentBoggleGame.class);
					i.putExtra(ISLOCAL, true);
					startActivity(i);			
				}
			}
			return null;
		}
		
		@Override
	    protected void onPreExecute() {
	        super.onPreExecute();
	        
			mProgressDialog.setMessage("Waiting for opponent to start game...");
			mProgressDialog.setIndeterminate(true);
	        mProgressDialog.show();
	    }

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			if(isInternet == true)
			{
				if(waitcounter >= 150)
				{
					Log.d(TAG, "Timeout : Wait for Timestamp");
					KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", player1.playerName);
					KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", player2.playerName);
					player2.playerName = "";
					player1.opponent = null;
					Message msg = new Message();
					msg.what = OpponentNotRespondedToast;
					handler.sendMessage(msg);
					// TODO :: can do startLocalGame
					//persbogglegame.islocal = false;
					Intent i = new Intent(context, PersistentBoggleGame.class);
					i.putExtra(ISLOCAL, true);
					
					startActivity(i);
					//persbogglegame.startLocalGame();
				}
				else if(waitcounter < 150)
				{
					Log.d(TAG, "----"+KeyValueAPI.get("Prachi_Pratik", "neu12345", player1.playerName + "_TS").trim()+"----");
					Log.d(TAG, "----"+KeyValueAPI.get("Prachi_Pratik", "neu12345", player2.playerName + "_TS").trim()+"----");
					Log.d(TAG, "----"+KeyValueAPI.get("Prachi_Pratik", "neu12345", player1.playerName + "_score").trim()+"----");
					Log.d(TAG, "----"+KeyValueAPI.get("Prachi_Pratik", "neu12345", player2.playerName + "_score").trim()+"----");
					
					player1.last_move = Long.valueOf(KeyValueAPI.get("Prachi_Pratik", "neu12345", player1.playerName + "_TS").trim());
					player2.last_move = Long.valueOf(KeyValueAPI.get("Prachi_Pratik", "neu12345", player2.playerName + "_TS").trim());
					player1.score = Integer.valueOf(KeyValueAPI.get("Prachi_Pratik", "neu12345", player1.playerName + "_score").trim());
					player2.score = Integer.valueOf(KeyValueAPI.get("Prachi_Pratik", "neu12345", player2.playerName + "_score").trim());
					persboggle = KeyValueAPI.get("Prachi_Pratik", "neu12345",  player2.playerName+"-"+player1.playerName).trim();
					Intent i = new Intent(context, PersistentBoggleGame.class);
					i.putExtra(ISLOCAL, false);
					startActivity(i);
				}
				mProgressDialog.dismiss();
			}

		}

		
	}
	public void waitforTimeStamp() {
		// TODO Auto-generated method stub
		 WaitForTimeStamp wts = new WaitForTimeStamp();
		 wts.execute();
	}
	
	
	public void initiateGame(String opponent, String possible_opponent){
		if(opponent.equals("") && possible_opponent.length() > 0)
		{
			// Didn't challenge anyone but challenged by new opponent
			Log.d(TAG, "initiateGame : Didn't challenge anyone but challenged by new opponent");
			challengeRequest(possible_opponent);
		}
		else if (opponent.equals(possible_opponent))
		{
			// Accepted request
			Log.d(TAG, "initiateGame : Accepted request");
			acceptRequest();
		}
		else if (!opponent.equals(possible_opponent))
		{
			//Request not accepted but got a new challenge
			Log.d(TAG, "initiateGame : Request not accepted but got a new challenge");
			twoRequest(possible_opponent);
		}
		else
			Log.d(TAG, "initiateGame function else block");
	}
	
	private void challengeRequest(String possible_opp) {
		// TODO Auto-generated method stub
		Log.d(TAG, "challengeRequest : reached challengeRequest");
		Bundle b = new Bundle();
		b.putString("opponent", possible_opp);
		Message msg = new Message();
		msg.setData(b);
		msg.what = ShowDialog;
		handler.sendMessage(msg);
	}

	public void twoRequest(String possible_opp) {
		Log.d(TAG, "two requests");
		Bundle b = new Bundle();
		b.putString("opponent", possible_opp);
		Message msg = new Message();
		msg.setData(b);
		msg.what = ShowDialogTwoRequest;
		handler.sendMessage(msg);
	}
	
	// Player 2 accepted challenge of player 1
	// task to be performed by player 1
	public void acceptRequest() {
		Log.d(TAG, "accept request");
		if (KeyValueAPI.isServerAvailable())
		{
			persboggle = persbogglegame.getNewBoggle();
			player1.last_move = System.currentTimeMillis();
			Log.d(TAG, "Player1 created boggle board: " + persboggle);			
			KeyValueAPI.put("Prachi_Pratik", "neu12345", player1.playerName+"_TS", Long.toString(player1.last_move));
			player2.last_move = System.currentTimeMillis();
			KeyValueAPI.put("Prachi_Pratik", "neu12345", player2.playerName+"_TS", Long.toString(player2.last_move));
			KeyValueAPI.put("Prachi_Pratik", "neu12345", player1.playerName + "-"+ player2.playerName, persboggle);
			//KeyValueAPI.put("Prachi_Pratik", "neu12345", player1.playerName+"_score", "0");
			//KeyValueAPI.put("Prachi_Pratik", "neu12345", player2.playerName+"_score", "0");
			player1.score = 0;
			player2.score = 0;
			KeyValueAPI.put("Prachi_Pratik", "neu12345", player1.playerName+"_score", Integer.toString(player1.score));
			KeyValueAPI.put("Prachi_Pratik", "neu12345", player2.playerName+"_score", Integer.toString(player2.score));
			
			//persbogglegame.startGame();
			//persbogglegame.islocal = true;
			Log.d(TAG, "Player1 info: " + player1.playerName + " " + player1.score + " " + player1.last_move);
			Log.d(TAG, "Player2 info: " + player2.playerName + " " + player2.score + " " + player2.last_move);
			Intent i = new Intent(context, PersistentBoggleGame.class);
			i.putExtra(ISLOCAL, false);
			startActivity(i);
		}
		else
		{
			//persbogglegame.islocal = false;
			Intent i = new Intent(context, PersistentBoggleGame.class);
			i.putExtra(ISLOCAL, true);
			startActivity(i);
		}
	}

	private void newGameBtnHandler() {
		// TODO Auto-generated method stub
		StringBuffer user_names = new StringBuffer(KeyValueAPI.get("Prachi_Pratik", "neu12345", "ListOfUsers").trim());
		Log.d(TAG, "All users :"+user_names);
		int start_index = user_names.indexOf(MainActivity.username);
		if (start_index !=-1)
			user_names.delete(start_index, start_index + MainActivity.username.length());
		StringBuffer user_array = new StringBuffer();
		Scanner sn = new Scanner(user_names.toString());
		sn.useDelimiter(",");
		while (sn.hasNext())
		{
			String str = sn.next().trim();
			
			if (!str.equals("") && 
					(KeyValueAPI.get("Prachi_Pratik", "neu12345", str).trim().equals("")) &&
					(KeyValueAPI.get("Prachi_Pratik", "neu12345", str +"_online").equals("true")))
			{
				user_array.append(str).append(",");
			}
		}
		Log.d(TAG, "Online users :"+user_array);
		final String [] users = user_array.toString().split(",");
		
		AlertDialog.Builder b = new Builder(this);
	    b.setTitle("Online users");
	    if (users.length ==0)
	    {
	    	b.setMessage("No users online");
	    	b.setPositiveButton("Play local game", new DialogInterface.OnClickListener() {

				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					Intent i = new Intent(context, PersistentBoggleGame.class);
					i.putExtra(ISLOCAL, true);
					startActivity(i);
				}
			});
	    	b.setNegativeButton("Exit", new DialogInterface.OnClickListener() {
				
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub
					KeyValueAPI.put("Prachi_Pratik", "neu12345", MainActivity.username+"_online", "false");
					finish();
				}
			});
	    }
	    else
	    {
	    b.setItems(users, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.dismiss();
				afterUserSelection(users, which);
			}

	    });
	    b.show();
		
	}
	}
	public void afterUserSelection(String[] users, int which) {
		// TODO Auto-generated method stub
		player2.playerName = users[which];
		KeyValueAPI.put("Prachi_Pratik", "neu12345", player2.playerName, player1.playerName);
		oppcounter = 0;
		Log.d(TAG, "opponent: "+player2.playerName);
		//Intent i = new Intent(this,PersistentBoggleGame.class);
		//i.putExtra(PersistentBoggleGame.KEY,1);
		//startActivity(i);
	}
}
