package edu.neu.madcourse.pratikdhandharia;

import java.util.List;

import com.everyfit.wockets.WocketsController;
import com.everyfit.wockets.WocketsService;
import com.everyfit.wockets.logging.Logger;
import com.everyfit.wockets.receivers.RFCOMMReceiver;
import com.everyfit.wockets.receivers.ReceiverStatus;
import com.everyfit.wockets.sensors.AndroidSensor;
import com.everyfit.wockets.sensors.Sensor;
import com.everyfit.wockets.sensors.SensorList;
import com.everyfit.wockets.utils.WocketsTimer;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.sax.RootElement;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

public class DataService extends Service
{
	private final String TAG = "DataService";
	private Looper mServiceLooper;
	private ServiceHandler mServiceHandler;
	private boolean running = true;
	public static final String PREF_FILE_NAME = "WocketsPrefFile";
	private String _ROOTPATH;
	private String _SENSORDATA;
	private Logger logger;
	private boolean serviceRunning = false;
	
	private final class ServiceHandler extends Handler
	{
		public ServiceHandler(Looper looper)
		{
			super(looper);
		}
		
		@Override
		public void handleMessage(Message msg)
		{
			int saveCounter=500;
			while(running)
			{
	            for (Sensor sensor:Application._Controller._Sensors)
	            {            	  
	            	sensor._Receiver.CheckStatus();
	            	if (sensor._Receiver._Status==ReceiverStatus.Connected)
	            	{
	            		sensor.Read();
	            	}
	            		
	            	else if (sensor._Receiver._Status==ReceiverStatus.Disconnected)
	            		//check no of reconnection attempts and then display error showing "Unable to connect to wockets"
	            	{
	            		//logger.log(sensor._ID, WocketsTimer.DateTimeFromUnixTime(System.currentTimeMillis()) + "Reconnecting");
	            		sensor.Reconnect();
	            	}
	            		            
	            }
	            
	            //Save and flush data infrequently
	            if (saveCounter--<0)
	            {
	            	for (Sensor sensor:Application._Controller._Sensors)
	            	{
	            		sensor.Save();            		
	            	}
	            		            	
	            	saveCounter=500;
	            }
	            
	            try{
	            	Thread.sleep(20);
	            }catch(Exception e){
	            	Log.e(TAG,"Exception in Data service");
	            	Log.e(TAG, e.getStackTrace().toString());
	            }
			}
			
			stopSelf(msg.arg1);
		}
	}
	
	@Override
	public void onCreate()
	{
		Application._Context = getApplicationContext();		
		
		SharedPreferences preferences = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		_ROOTPATH = preferences.getString("ROOTPATH", "");
		_SENSORDATA = preferences.getString("SENSORDATA", "");
		running = preferences.getBoolean("running", false);
		
		if(running == false)
			stopSelf();
		else
		{
			Application._running = true;
			serviceRunning = true;
			HandlerThread thread = new HandlerThread("CollectDataService",Process.THREAD_PRIORITY_DEFAULT);
			thread.start();
			mServiceLooper = thread.getLooper();
			mServiceHandler = new ServiceHandler(mServiceLooper);
		}
		
	
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId)
	{
		SharedPreferences preferences = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE);
		SharedPreferences.Editor editor= preferences.edit();
		
		_ROOTPATH = preferences.getString("ROOTPATH", "");
		_SENSORDATA = preferences.getString("SENSORDATA", "");
		
		running = preferences.getBoolean("running", false);
		//logger = new Logger(_ROOTPATH);
		
		if(running == true && serviceRunning == true)
		{
			startSession();
		}
		else
		{
			// terminate the service
			stopSelf();
			editor.clear();
			editor.commit();
			stopSelf();
		}
		
		Message msg  = mServiceHandler.obtainMessage();
		msg.arg1 = startId;
		mServiceHandler.sendMessage(msg);
		
		//restart if killed 
		return START_NOT_STICKY;
		//return START_STICKY;
	}
	
	@Override
	public void onDestroy()
	{
		//Toast.makeText(this, "Data Collection Terminated", Toast.LENGTH_SHORT).show();
		running = false;
		Application._running = false;
		
		SharedPreferences preferences = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.clear();
		editor.commit();
		
		super.onDestroy();
	}
	
	@Override
	public IBinder onBind(Intent arg0) {
		// does not provide binding
		return null;
	}
	
	public void startSession()
	{		
		
		//if(running == false)
		{
			running = true;
			SharedPreferences preferences = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE);		
			SharedPreferences.Editor editor = preferences.edit();
			editor.putBoolean("running", running);			
			editor.commit();
			_ROOTPATH = preferences.getString("ROOTPATH", "");
			_SENSORDATA = preferences.getString("SENSORDATA", "");
			
			Application._Context = this.getApplicationContext();
			Application._Wockets = new Wockets();
			Application._Controller= new WocketsController();
			WocketsService._Context=Application._Context;
			WocketsService._Controller=Application._Controller;
			Application._Controller.Initialize(_SENSORDATA);
			Application._running = true;
			
			Application._Controller.FromXML(this._ROOTPATH + _SENSORDATA);
			SensorList sensorList = Application._Controller._Sensors;
			BluetoothAdapter.getDefaultAdapter().enable();
			for(int i = 0 ; i < sensorList.size(); i++)
			{
				Application._Controller._Sensors.get(i).Initialize();

				try 
				{
					Thread.sleep(1000);
				}
				catch (InterruptedException e) 
				{					
					Log.e(TAG, "Unable to sleep");
				}
				
				if(sensorList.get(i)._Class.equalsIgnoreCase("Wockets"))
					Application._Wockets.add(new Wocket(((RFCOMMReceiver)sensorList.get(i)._Receiver)._Address, sensorList.get(i)._ID));
					
			}
			
			if (Application._Wockets.size()>0)			
				Application._Controller.Connect(Application._Wockets.toAddressArrayList());
			 else
			 {
				 Toast.makeText(this, "Unable to connect to wockets", Toast.LENGTH_LONG).show();
				 return;
			 }		 
		        
			
		}
	}

}


