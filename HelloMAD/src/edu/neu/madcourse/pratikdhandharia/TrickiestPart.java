package edu.neu.madcourse.pratikdhandharia;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TrickiestPart extends Activity {
	final Context context = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trickiestpart);
		
		Button level1 = (Button) findViewById(R.id.level1);
		Button level2 = (Button) findViewById(R.id.level2);
		
		level1.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i1 = new Intent(context, Trickiestpart1.class);
				startActivity(i1);
			}
		});
		
		level2.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i1 = new Intent(context, Trickiestpart2.class);
				startActivity(i1);
			}
		});
		
	}

}
