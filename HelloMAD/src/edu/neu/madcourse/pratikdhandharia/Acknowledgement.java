package edu.neu.madcourse.pratikdhandharia;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class Acknowledgement extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.acknowledgement);
		
		TextView txtack = (TextView) findViewById(R.id.txtacknowledgement);
		txtack.setText(getIntent().getStringExtra("ack"));
		setTitle("Acknowledgements");
	}
}
