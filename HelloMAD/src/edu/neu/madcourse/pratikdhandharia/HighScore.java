package edu.neu.madcourse.pratikdhandharia;

import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.os.Bundle;
import android.text.Layout;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import edu.neu.mobileclass.apis.KeyValueAPI;

public class HighScore extends Activity {
	
	public class scoreRecord{
		String name,score,dates;
		
		public scoreRecord(String n,String s,String d)
		{
			name = n;
			score = s;
			dates = d;
		}
	}

	TableLayout t;
	TableRow tr;
	TextView name,score,date;
	int currScore;
	Button btn;
	
	private int width;    // width of one tile
	
	@Override
	public void onBackPressed() {
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.highscore);
//		Log.d("highscore", "step 1");
		t = (TableLayout) findViewById(R.id.tblhighscore);
		
		TableLayout.LayoutParams tableRowParams=
				  new TableLayout.LayoutParams
				  (TableLayout.LayoutParams.FILL_PARENT,TableLayout.LayoutParams.WRAP_CONTENT);

		int leftMargin=2;
		int topMargin=2;
		int rightMargin=2;
		int bottomMargin=2;

		tableRowParams.setMargins(leftMargin, topMargin, rightMargin, bottomMargin);
		
//		Log.d("highscore", "step 2");
		btn = new Button(this);

		Display display = getWindowManager().getDefaultDisplay();
		width = display.getWidth();
		
		btn.setText("Finish!!");
//		Log.d("highscore", "step 3");
		btn.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		
		Date d = new Date();
		currScore = getIntent().getIntExtra("SCORE",0);
//		Log.d("highscore", "step 4");
		String s = KeyValueAPI.get("Prachi_Pratik", "neu12345", "HighScores");
		String final_s = "";
		if(s.equals(""))
		{
//			Log.d("highscore", "step 5");
			//name.setText("No High Scores.");
			s = MainActivity.username + "," + currScore + "," + d.toGMTString();
			KeyValueAPI.put("Prachi_Pratik", "neu12345", "HighScores",s);
			name = new TextView(this);
			name.setText(MainActivity.username + " | ");
			name.setWidth(width/3);
			
			score = new TextView(this);
			score.setText(currScore + " | ");
			score.setWidth(width/3);
			
			date = new TextView(this);
			date.setText(d.toGMTString());
			date.setWidth(width/3);
			
			tr = new TableRow(this);
			tr.addView(name);
			tr.addView(score);
			tr.addView(date);
			tr.setLayoutParams(tableRowParams);
			
			t.addView(tr);
			
//			Log.d("HighScore", s);
		}
		else
		{
//			Log.d("highscore", "step 6");
			String[] highscore = s.split(",");

			ArrayList<scoreRecord> sr = new ArrayList<scoreRecord>();
			
//			Log.d("highscore", "step 7 -----" + highscore.length);
			
			for(int i =0; i<highscore.length ; i=i+3)
			{
				sr.add(new scoreRecord(highscore[i].trim(), highscore[i+1].trim(), highscore[i+2].trim()));
			}
			int i;
			for(i=0;i<sr.size(); i++)
			{
				if(currScore > Integer.parseInt(sr.get(i).score))
					break;
			}
			if(i<sr.size())
			{
//				sscore.set(i, currScore);
//				sname.set(i, MainActivity.username);
//				sdate.set(i, d.toGMTString());
				
				sr.add(i, new scoreRecord(MainActivity.username, String.valueOf(currScore), d.toGMTString()));
				for(i=0;i<sr.size(); i++)
				{
					final_s += sr.get(i).name + "," + sr.get(i).score + "," + sr.get(i).dates + ",";
				}
				final_s = final_s.substring(0, final_s.length()-1);
				KeyValueAPI.put("Prachi_Pratik", "neu12345", "HighScores",final_s);
			}
			for(i=0;i<sr.size(); i++)
			{
				//final_s += sname.get(i) + "," + String.valueOf(sscore.get(i)) + "," + sdate.get(i)+ ",";
				name = new TextView(this);
				name.setText(sr.get(i).name + " | ");
				
				score = new TextView(this);
				score.setText(sr.get(i).score + " | ");
				
				date = new TextView(this);
				date.setText(sr.get(i).dates);
				
				tr = new TableRow(this);
				tr.addView(name);
				tr.addView(score);
				tr.addView(date);
				tr.setLayoutParams(tableRowParams);
				t.addView(tr);
			}
			tr = new TableRow(this);
			tr.addView(btn);
			tr.setLayoutParams(tableRowParams);
			t.addView(tr);
//			Log.d("HighScore", final_s);
		}
	}

	
	
	
}
