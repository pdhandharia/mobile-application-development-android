package edu.neu.madcourse.pratikdhandharia;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import edu.neu.mobileclass.apis.KeyValueAPI;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class PersistentBoggleGame  extends Activity implements OnClickListener{

	private static final String TAG = "PersistentBoggleGame";
	private static final String KEY_OPPONENT_SCORE = "opponentscore";
	private static final int UpdateOpponentdata = 1;
	private static final int RemoveOpponentdata = 2;

	public static final String KEY = "1";
	public static boolean islocal = false;
	MediaPlayer mp_letter,mp_successful;
	Vibrator vibrator;
	public Bundle pers_wordlist_data = new Bundle();
	public Bundle pers_wordlistindex_data = new Bundle();
	public Bundle pers_score_data = new Bundle();

	public static final int CONTINUE = 0;
	public static final int NEW_GAME = 1;

//	public String persboggle;
	private ArrayList<String> persboggleWords;
	public ArrayList<Integer> persbogglewordsindex;
	public String currentWord;
	public int score;
	public int opponent_score;
	Timer mygametimer;
	Handler updateplayer2data;
	
	private long pauseTimer = 0;
	private int startTimer = 60000;
	private int timeInterval = 1000;
	private Mycountdowntimer mycountdown;
	public static String opponent;

	private PersistentBoggleView persboggleview;

	LayoutParams l;
	TableLayout boggletable;
	TextView txtTimer, txtCurrentWord, txtplayer1score, txtplayer2score;
	Button btnbogglegamepause, btnbogglecheckword, btnlistofwords, btnpersboggleDelete;
	TableRow bogglegridrow;

	int height, width;
	int grid_start;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.persistentbogglegame);
		
		//int key = 1;
				//getIntent().getIntExtra(KEY, NEW_GAME);

		//persboggle = getBoggle(key);
		
		//handler to update player 2 score from the timer
		updateplayer2data = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				switch(msg.what)
				{
				case UpdateOpponentdata:
					txtplayer2score.setText(PersistentBoggle.player2.playerName + "'s score :" + msg.getData().getString(KEY_OPPONENT_SCORE));
					break;
				case RemoveOpponentdata:
					Toast t = Toast.makeText(getApplicationContext(), "Either lost internet connection or Opponent left the game.", Toast.LENGTH_SHORT);
					t.show();
					txtplayer2score.setEnabled(false);
					txtplayer2score.setText("");
					break;
				}
			}
		};
		islocal =  getIntent().getBooleanExtra("ISLOCAL",true);
		startGame();
		
		mp_letter = MediaPlayer.create(this, R.raw.letter_click);
		mp_successful = MediaPlayer.create(this,R.raw.successful_word);
		vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
		
		boggletable = (TableLayout) findViewById(R.id.persboggletablelayout);
		bogglegridrow = new TableRow(this);

		txtTimer = (TextView) findViewById(R.id.txtperstimer);
		txtCurrentWord = (TextView) findViewById(R.id.txtpersCurrentWord);
		btnbogglegamepause = (Button) findViewById(R.id.btnpersbogglePause);
		btnbogglecheckword = (Button) findViewById(R.id.btnperscheckword);
		btnpersboggleDelete = (Button) findViewById(R.id.btnpersboggleDelete);
		btnlistofwords = (Button) findViewById(R.id.btnpersbogglewords);
		txtplayer1score = (TextView) findViewById(R.id.txtplayer1score);
		txtplayer2score = (TextView) findViewById(R.id.txtplayer2score);
		
		// on click for pause button
		btnbogglegamepause.setOnClickListener(this);

		btnbogglecheckword.setOnClickListener(this);

		btnlistofwords.setOnClickListener(this);
		
		btnpersboggleDelete.setOnClickListener(this);

		Display display = getWindowManager().getDefaultDisplay();
		height = display.getHeight();
		width = display.getWidth();
		grid_start = (int) (height * 0.3);

		txtTimer.setWidth(width / 2);
		txtTimer.setHeight(grid_start / 3);
		txtCurrentWord.setWidth(width / 2);
		txtCurrentWord.setHeight(grid_start / 3);
		txtCurrentWord.setText(currentWord);
		
		txtplayer1score.setWidth(width / 2);
		txtplayer1score.setHeight(grid_start / 3);
		txtplayer1score.setText(PersistentBoggle.player1.playerName + "'s score : " + String.valueOf(score));
		
		txtplayer2score.setWidth(width / 2);
		txtplayer2score.setHeight(grid_start / 3);
		if (islocal != true)
		{
			txtplayer2score.setText(PersistentBoggle.player2.playerName + "'s score : " + String.valueOf(opponent_score));
		}
		
		btnbogglegamepause.setText("Pause");
		btnbogglecheckword.setText("Check");
		btnpersboggleDelete.setText("Backspace");
		btnlistofwords.setText("List Of Words");
		l = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		persboggleview = new PersistentBoggleView(this);

		TableRow.LayoutParams boggleviewlayout = new TableRow.LayoutParams();

		boggleviewlayout.span = 2;
		persboggleview.setLayoutParams(boggleviewlayout);

		bogglegridrow.addView(persboggleview);

		// boggleview.setLayoutParams()
		boggletable.addView(bogglegridrow, l);
		persboggleview.requestFocus();
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		//clearKeys();
		btnpausehandler();
		
		// Log.d(TAG,"" + String.valueOf(boggleWords.size()));
	}
	
	private void btnpausehandler()
	{
		clearLocaldata();
		if(mycountdown !=null) mycountdown.cancel();
		if(mygametimer!= null) mygametimer.cancel();
		
		KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player1.playerName);
		//KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player1.playerName+"-" + PersistentBoggle.player2.playerName);
		//KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player2.playerName+"-" + PersistentBoggle.player1.playerName);
		KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player1.playerName+ "_TS");
		KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player1.playerName+ "_score");
		finish();
	}
	
	
	// function to get a character at particular location
	public String getStringAt(int i) {
		return PersistentBoggle.persboggle.substring(i, i + 1);
	}

	// function to check if the current letter is already clicked
	public Boolean isalreadyclicked(int i) {
		if (persbogglewordsindex.size() > 0)
			return persbogglewordsindex.contains(i);
		else
			return false;
	}
	
	public int getlastclicked()
	{
		if (persbogglewordsindex.size() <=0)
			return -1;
		return persbogglewordsindex.get(persbogglewordsindex.size()-1);
	}
	
	public void handlefinishtime()
	{
		txtTimer.setText("Time Over");
		mycountdown.cancel();
	
		Intent i = new Intent(this, HighScore.class);
		i.putExtra("SCORE", score);
		clearKeys();
		clearLocaldata();
		startActivity(i);
//		AlertDialog ad = new AlertDialog.Builder(this).create();
//		ad.setTitle(MainActivity.username+ "'s score: " + score);
//		String s = "";
//		for (int i = 0; i < persboggleWords.size(); i++) {
//			s += persboggleWords.get(i);
//			if(i != persboggleWords.size() - 1)
//			{
//				s += "-";
//			}
//		}
//		ad.setMessage(s);
//		ad.setCancelable(false);
//		ad.setButton("Finish!!", new DialogInterface.OnClickListener() {
//			
//			public void onClick(DialogInterface dialog, int which) {
//				// TODO Auto-generated method stub
//				clearKeys();
//				clearLocaldata();
//				finish();
//				
//			}
//		});
//		ad.show();
	}

	// Count down timer class
	public class Mycountdowntimer extends CountDownTimer {

		public Mycountdowntimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			//Log.d(TAG, "pause timer value: " + String.valueOf(pauseTimer));
			if(mygametimer != null) mygametimer.cancel();
			handlefinishtime();

		}

		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub
			pauseTimer = millisUntilFinished;
			txtTimer.setText("Time Remaining: "
					+ String.valueOf(millisUntilFinished / 1000));
		}
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnpersbogglePause:
			//clearKeys();
			btnpausehandler();
			break;
		case R.id.btnperscheckword:
			btncheckwordhandler();
			break;
		case R.id.btnpersbogglewords:
			btnlistofWordshandler();
			break;
		case R.id.btnpersboggleDelete:
			btnpersboggleDetelehandler();
			break;
			
		}
	}

	private void btnpersboggleDetelehandler() {
		// TODO Auto-generated method stub
		if(currentWord.length()>=1)
		{
 		currentWord = currentWord.substring(0, currentWord.length()-1);
		persbogglewordsindex.remove(persbogglewordsindex.size()-1);
		txtCurrentWord.setText(currentWord);
		}
		
	}

	// handler for button list of words
	public void btnlistofWordshandler() {
		AlertDialog ad = new AlertDialog.Builder(this).create();
		String s = "";
		for (int i = 0; i < persboggleWords.size(); i++) {
			s += persboggleWords.get(i);
			if(i != persboggleWords.size() - 1)
			{
				s += "-";
			}
		}
		ad.setMessage(s);
		ad.show();
	}

	// handler for btn check
	public void btncheckwordhandler() {
		// TODO check for blank word, invalid word, same word
		// TODO also add logic for calculating points.
		if (currentWord.length() > 0) {
			if (persboggleWords.contains(currentWord)) {
				Toast duplicate = Toast.makeText(this, "duplicate word",
						Toast.LENGTH_SHORT);
				duplicate.show();
			} else {
				if (isvalidWord()) {
					calculateScore(currentWord);
					
					// Adding word to user's word list.
					persboggleWords.add(currentWord);

					// updating the score textview
					txtplayer1score.setText(MainActivity.username+ "'s score: " + String.valueOf(score));
					AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
					switch(am.getRingerMode())
					{
					case AudioManager.RINGER_MODE_SILENT:
					case AudioManager.RINGER_MODE_VIBRATE:
						vibrator.vibrate(100);
						break;
					case AudioManager.RINGER_MODE_NORMAL:
						mp_successful.start();
						break;
					}
						
				}
			}
			// resetting the Current word to blank
			currentWord = "";
			txtCurrentWord.setText("");

			// Reseting the selected word list
			persbogglewordsindex = new ArrayList<Integer>();
		}
	}


	public boolean isvalidWord() {
		boolean result = false;
		try {
			
			String str = "";
			int len = currentWord.length();
			char firstChar = currentWord.charAt(0);
			
			InputStream is = this.getAssets().open(String.valueOf(len) + "/" + firstChar + ".txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(is));

			while((str = br.readLine()) != null)
			{
				//Log.d(TAG, str);
				if (str.trim().equals(currentWord))
				{
					result = true;
					break;
				}
			}
			br.close();
			is.close();
		} catch (Exception e) {
			Log.d(TAG, "reading from file: " + e.getMessage());
		}
		return result;
	}

	// calculate and update the score
	public void calculateScore(String currentword) {
		if (currentword.length() >= 3)
		{
			score += currentword.length();
			KeyValueAPI.put("Prachi_Pratik", "neu12345", PersistentBoggle.player1.playerName+"_score", Integer.toString(score));
		}
	}

	// function to generate new boggle board
	
	public String getNewBoggle() {
		String s = "";
		String game[] = {"aaafrs","aaeeee","aafirs","adennn","aeeeem","aeegmu","aegmnn","afirsy","bjkqxz","ccnstw","ceiilt","ceilpt","ceipst",
				"ddlnor","dhhlor","dhhnot","dhlnor","eiiitt","emottt","ensssu","fiprsy","gorrvw","hiprry","nootuw","ooottu"};
		int random = 0;
		for (int i = 0; i < game.length; i++) {
			random = (int) ((Math.random() * 10 + 1) % 6);
			s += game[i].substring(random, random + 1);
		}
		return s;
	}
	
	public void startOnlineGame(){
		//Log.d(TAG, "start Game "+PersistentBoggle.player1.playerName+" "+PersistentBoggle.player2.playerName);
		//persboggle = KeyValueAPI.get("Prachi_Pratik", "neu12345",  PersistentBoggle.player2.playerName+"-"+PersistentBoggle.player1.playerName);
		//Log.d(TAG, "boggle board online: " + persboggle);
		mygametimer = new Timer();
		mygametimer.scheduleAtFixedRate(new TimerTask() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				if(KeyValueAPI.isServerAvailable())
				{
					// online update score 
					
					PersistentBoggle.player1.last_move = System.currentTimeMillis();
	
					KeyValueAPI.put("Prachi_Pratik", "neu12345", PersistentBoggle.player1.playerName+"_TS", 
							Long.toString(PersistentBoggle.player1.last_move));
					
					// read timestamp of opponent
					Long curr_ts;
					String player2_TS = KeyValueAPI.get("Prachi_Pratik", "neu12345", 
							PersistentBoggle.player2.playerName +"_TS").trim(); 
					if(player2_TS.equals(""))
					{
						Log.d(TAG,"start online game inside if " + "------" + player2_TS +"---------");
						curr_ts = PersistentBoggle.player2.last_move;
					}
					else
					{
						Log.d(TAG,"start online game inside else" + "------" + player2_TS +"---------");
						curr_ts =Long.parseLong(player2_TS);
					}
					
					if (curr_ts != PersistentBoggle.player2.last_move)
					{
						PersistentBoggle.player2.last_move = curr_ts;
						String player2_score = KeyValueAPI.get("Prachi_Pratik", "neu12345", 
								PersistentBoggle.player2.playerName+"_score").trim();
						if(player2_score.equals(""))
						{
							opponent_score = PersistentBoggle.player2.score;
						}
						else
						{
							Log.d(TAG, "opponent score in run: " + player2_score);
							opponent_score = Integer.parseInt(player2_score);							
						}
						PersistentBoggle.player2.score = opponent_score;
						//txtplayer2score.setText(PersistentBoggle.player2.playerName+"'s score : " + 
						//		String.valueOf(opponent_score));
						Bundle data = new Bundle();
						data.putString(KEY_OPPONENT_SCORE, String.valueOf(opponent_score));
						Message msg = new Message();
						msg.what = UpdateOpponentdata;
						msg.setData(data);
						updateplayer2data.sendMessage(msg);
					}
					else
					{
						Bundle data = new Bundle();
						data.putString(KEY_OPPONENT_SCORE, "");
						Message msg = new Message();
						msg.what = RemoveOpponentdata;
						msg.setData(data);
						updateplayer2data.sendMessage(msg);
						
						clearKeys();
						Log.d(TAG, "player 2 not updating the timestamp.");
						mygametimer.cancel();
					}
					// check timestamp of opponent (if changed then opponent is online)
				}
				else
				{
					Bundle data = new Bundle();
					data.putString(KEY_OPPONENT_SCORE, "");
					Message msg = new Message();
					msg.what = RemoveOpponentdata;
					msg.setData(data);
					updateplayer2data.sendMessage(msg);
					mygametimer.cancel();
				}
			}
		}, 0, 500);
		clearLocaldata();
		mycountdown = new Mycountdowntimer(startTimer, timeInterval);
		mycountdown.start();
	}
	public void startGame(){
		Log.d(TAG, "start Game");
		Log.d(TAG, "boggle board" + PersistentBoggle.persboggle);
		if(islocal == true)
		{
			PersistentBoggle.persboggle = getNewBoggle();
			mycountdown = new Mycountdowntimer(startTimer, timeInterval);
			mycountdown.start();
			clearLocaldata();
		}
		else startOnlineGame();
	}
	
	public void clearKeys(){
		if(KeyValueAPI.isServerAvailable())
		{
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player2.playerName+"_TS");
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player1.playerName+"_TS");
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player1.playerName + "-"+ PersistentBoggle.player2.playerName);
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player2.playerName + "-"+ PersistentBoggle.player1.playerName);
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player1.playerName+"_score");
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player2.playerName+"_score");
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player1.playerName);
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", PersistentBoggle.player2.playerName);
		}
	}

	private void clearLocaldata() {
		currentWord = "";
		pauseTimer = startTimer;
		persboggleWords = new ArrayList<String>();
		persbogglewordsindex = new ArrayList<Integer>();
		score = 0;
	}
	
}
