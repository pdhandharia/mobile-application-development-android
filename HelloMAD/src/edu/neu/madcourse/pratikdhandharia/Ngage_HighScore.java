package edu.neu.madcourse.pratikdhandharia;

import edu.neu.mobileclass.apis.KeyValueAPI;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.TextView;

public class Ngage_HighScore extends Activity{
	
	private static final int UPDATESCORE = 0;
	private static final String SCORE = "score";
	TextView levelno,myhighscore,onlinehighscore;
	Handler handler;
	ProgressDialog mProgressDialog;
	LoadingData ld;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ngage_highscore);
		
		levelno = (TextView) findViewById(R.id.highscoreLevelnumber);
		levelno.setTextSize(20f);
		levelno.setTextColor(Color.BLACK);

		myhighscore = (TextView) findViewById(R.id.myhighscore);
		myhighscore.setTextSize(20f);
		myhighscore.setTextColor(Color.BLACK);

		onlinehighscore = (TextView) findViewById(R.id.onlinehighscore);
		onlinehighscore.setTextSize(20f);
		onlinehighscore.setTextColor(Color.BLACK);
		
		mProgressDialog = new ProgressDialog(this);
		
		levelno.setText("Level Number: " + String.valueOf(NgageMainActivity.user.current_level));
		
		if(NgageMainActivity.user.scores.containsKey(NgageMainActivity.user.current_level))
		{
			myhighscore.setText("Your high score : " + NgageMainActivity.user.scores.get(NgageMainActivity.user.current_level));
		}
		else
		{
			myhighscore.setText("You haven't played this level!!");			
		}
//		myhighscore.setTextSize(20f);

		handler = new Handler(){

			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				switch(msg.what)
				{
				case UPDATESCORE:
					onlinehighscore.setText(msg.getData().getString(SCORE));
					break;
				}
			}
			
		};
		
		ld = new LoadingData();
		ld.execute();		

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	
	class LoadingData extends AsyncTask<Void,Void,Void>
	{

		Bundle data = new Bundle();
		Message msg = new Message();
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			if(KeyValueAPI.isServerAvailable())
			{
				String s = KeyValueAPI.get("Prachi_Pratik", "neu12345", "level_" + NgageMainActivity.user.current_level + "_hs");
				if(s.equals(""))
				{
					data.putString(SCORE, "Online High score : No High Score Yet!!");
					//onlinehighscore.setText("Online High score : No High Score Yet!!");
				}
				else
				{
					String[] s1 = s.split(",");
					//onlinehighscore.setText("Online High score by " + s1[0] + " is " + s1[1]);
					data.putString(SCORE, "Online High score by " + s1[0] + " is " + s1[1]);
		 		}
			}
			else
			{
				//onlinehighscore.setText("Online High score : Internet unavailable!!");
				data.putString(SCORE, "Online High score : Internet unavailable!!");
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
			mProgressDialog.setMessage("Loading data...");
			mProgressDialog.setIndeterminate(true);
	        mProgressDialog.show();
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			msg.what = UPDATESCORE;
			msg.setData(data);
			handler.handleMessage(msg);
			mProgressDialog.dismiss();
		}
		
		
		
		
	}
}
