package edu.neu.madcourse.pratikdhandharia;

import edu.neu.mobileclass.apis.KeyValueAPI;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class SignUp extends Activity implements OnClickListener {
	public static final String NEWUSER = "newuser";
	public static final String NGAGEUSER = "ngageuser";
	public static final String NEWUSERPASSWORD = "newuserpassword";
	public static final String NGAGEUSERPASSWORD = "ngageuserpassword";
	
	Button btnsignup;
	Button btncancel;
	TextView textusername;
	EditText editusername;
//	TextView textpassword;
//	EditText editpassword;
	int height, width;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup);
		
		btnsignup = (Button) findViewById(R.id.btnsignup);
		btncancel = (Button) findViewById(R.id.btncancel);
		textusername = (TextView) findViewById(R.id.txtuser);
		editusername = (EditText) findViewById(R.id.edituser);
//		textpassword = (TextView) findViewById(R.id.txtpwd);
//		editpassword = (EditText) findViewById(R.id.editpwd);
		
		Display display = getWindowManager().getDefaultDisplay();
		height = display.getHeight();
		width = display.getWidth();

		textusername.setWidth((int) ((width*0.8)/ 2));
		editusername.setWidth((int) ((width*0.8)/ 2));
		btnsignup.setWidth((int) ((width*0.8)/ 2));
		btncancel.setWidth((int) ((width*0.8)/ 2));
//		textpassword.setWidth(width / 2);
//		editpassword.setWidth(width / 2);
		
		btnsignup.setOnClickListener(this);
		btncancel.setOnClickListener(this);
	}
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.btnsignup:
			btnSignupHandler();
			break;
		case R.id.btncancel:
			finish();
			break;
		}
	}
//	private void btnSignupHandler() {
//		// TODO Auto-generated method stub
//		try
//		{
//		String un = editusername.getEditableText().toString();
////		String pwd = editpassword.getEditableText().toString();
//		// TODO Auto-generated method stub
//		if(!un.equals(""))
////			&& !pwd.equals(""))
//		{
//			if (KeyValueAPI.isServerAvailable())
//			{
//				String password = KeyValueAPI.get("Prachi_Pratik", "neu12345", un+"_password");
//				if (password.trim().equals(pwd))
//				{
//					saveUserCredentials(un, pwd);
//					NgageMainActivity.user.username = un;
//					NgageMainActivity.user.password = pwd;
//				}
//				else
//				{
//					String listofusers = KeyValueAPI.get("Prachi_Pratik", "neu12345", "NGAGElistofusers");
//					Log.d("Sign Up", listofusers);
//					if (listofusers.contains(un))
//					{
//						Toast.makeText(this, "Username already exists/Password incorrect!", Toast.LENGTH_SHORT).show();
//					}
//					else
//					{
//						if (listofusers.equals(""))
//							listofusers = listofusers.concat(un);
//						else
//							listofusers = listofusers.concat(","+un);
//						KeyValueAPI.put("Prachi_Pratik", "neu12345", "NGAGElistofusers", listofusers);
//						KeyValueAPI.put("Prachi_Pratik", "neu12345", un+"_password",pwd);
//						saveUserCredentials(un, pwd);
//						NgageMainActivity.user.username = un;
//						NgageMainActivity.user.password = pwd;
//					}
//				}
//			}
//			else
//			{
//				Toast.makeText(this, "Internet not available!!", Toast.LENGTH_SHORT).show();
//			}
//			
//		}
//		else
//		{
//			Toast.makeText(this, "Both the fields are necessary!!", Toast.LENGTH_SHORT).show();
//		}
//		}
//		catch (Exception e)
//		{
//			e.printStackTrace();
//		}
//	}
	
	private void btnSignupHandler(){
		String un = editusername.getEditableText().toString();
		if(!un.equals(""))
		{
			if(KeyValueAPI.isServerAvailable())
			{
				String listofusers = KeyValueAPI.get("Prachi_Pratik", "neu12345", "NGAGElistofusers");
				if (listofusers.contains(un))
					{
						//Login
						saveUserCredentials(un);
						Intent i = new Intent(this,Ngage.class);
						startActivity(i);
					}
					else
					{
						if (listofusers.equals(""))
							listofusers = listofusers.concat(un);
						else
							listofusers = listofusers.concat(","+un);
						KeyValueAPI.put("Prachi_Pratik", "neu12345", "NGAGElistofusers", listofusers);
						//KeyValueAPI.put("Prachi_Pratik", "neu12345", un+"_password",pwd);
						saveUserCredentials(un);
						NgageMainActivity.user.username = un;
						//NgageMainActivity.user.password = pwd;
					}
			}
			else
			{
				Toast.makeText(this, "Internet Unavailable.", Toast.LENGTH_LONG).show();				
			}
		}
		else
		{
			Toast.makeText(this, "Please enter a name", Toast.LENGTH_LONG).show();
		}
	}
	
	private void saveUserCredentials(String un) {
		// TODO Auto-generated method stub
		getSharedPreferences(NEWUSER, Activity.MODE_PRIVATE).edit()
		.putString(NGAGEUSER,un)
		.commit();
		
//		getSharedPreferences(NEWUSERPASSWORD, Activity.MODE_PRIVATE).edit()
//		.putString(NGAGEUSERPASSWORD,pwd)
//		.commit();
		
		NgageMainActivity.user.username = un;
		//NgageMainActivity.user.password = pwd;

		Intent i = new Intent(this,Ngage.class);
		startActivity(i);

	}
}

