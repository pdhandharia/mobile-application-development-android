package edu.neu.madcourse.pratikdhandharia;


import java.io.PrintWriter;
import java.io.StringWriter;

import com.everyfit.wockets.WocketsController;
import com.everyfit.wockets.WocketsService;
import com.everyfit.wockets.kernel.KernelCallback;
import com.everyfit.wockets.kernel.KernelListener;
import com.everyfit.wockets.kernel.KernelMessage;
import com.everyfit.wockets.logging.Logger;
import com.everyfit.wockets.receivers.RFCOMMReceiver;
import com.everyfit.wockets.receivers.ReceiverStatus;
import com.everyfit.wockets.sensors.Sensor;
import com.everyfit.wockets.sensors.SensorList;


import android.app.IntentService;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class CollectDataService extends IntentService
{
	private final String TAG = "CollectDataService";
	public static final String PREF_FILE_NAME = "WocketsPrefFile";
	
	private boolean serviceRunning = false;
	private boolean running = true;
	final Handler mHandler = new Handler();
	private String _ROOTPATH;
	private String _SENSORDATA;
	
	public CollectDataService() 
	{
		
		super("CollectDataService");		
		setIntentRedelivery(true);
	}
	
	@Override
	public void onCreate()
	{	
		SharedPreferences preferences = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE);


		_ROOTPATH = preferences.getString("DATASTORAGEPATH", "");
		_SENSORDATA = preferences.getString("SENSORDATA", "");
		running = preferences.getBoolean("running", false);		
		if(running == false)
		{
			stopSelf();	
		}
		Log.d(TAG, "running in data service=" + running);
		//Toast.makeText(Application._Context, "Starting Data Collection", Toast.LENGTH_LONG).show();
		
		super.onCreate();
	}

	@Override
	protected void onHandleIntent(Intent intent) 
	{
		SharedPreferences preferences = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE);

		_ROOTPATH = preferences.getString("DATASTORAGEPATH", "");
		_SENSORDATA = preferences.getString("SENSORDATA", "");
		running = preferences.getBoolean("running", false);
		//logger = new Logger(_ROOTPATH);
				
//		Logger.debug("Starting Data Colection Service");
		startSession();
		
//		if(running & serviceRunning == false)
//		{
//			//startSession();
//		}
		int saveCounter=500;
		while(running && Application._running)		
		{
			serviceRunning = true;
            for (Sensor sensor:Application._Controller._Sensors)
            {            	  
            	sensor._Receiver.CheckStatus();
            	if (sensor._Receiver._Status==ReceiverStatus.Connected)
            	{
            		sensor.Read();
            	}            		
            	else if (sensor._Receiver._Status==ReceiverStatus.Disconnected)            	
            	{            		                         
                    //logger.log(this._ID,_DateFormat_log.format(logTmpDt) + "," + data._X + "," + data._Y + "," + data._Z);
            		//Logger.debug(((RFCOMMReceiver)sensor._Receiver)._Address + " ,Reconnecting");
              		sensor.Reconnect(); 
            	}            		        				        		
            }
            
            //Save and flush data infrequently
            if (saveCounter--<0)
            {
            	for (Sensor sensor:Application._Controller._Sensors)
            	{
            		if (sensor._Receiver._Status==ReceiverStatus.Connected)
            			sensor.Save();            		
            	}
            		            	
            	saveCounter=500;
            }
            
            try
            {
            	Thread.sleep(20);
            }
            catch(InterruptedException e)
            {
            	String strMsg = TAG + ":Main Thread\n";
				strMsg += e.getMessage() + "\n";
				
				StringWriter errTrace = new StringWriter();
				e.printStackTrace(new PrintWriter(errTrace));
				
				strMsg += errTrace.toString();
				
		//		Logger.error(strMsg);
            }
		}
	}
	
	
	
	@Override
	public void onDestroy()
	{
	//	Logger.debug("Data Colection Service Terminated");
		
		SharedPreferences preferences = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE);
		SharedPreferences.Editor editor = preferences.edit();
		editor.clear();
		editor.commit();
		
		super.onDestroy();
	}
	
	public void startSession()
	{		
		
		//if(running == false)
		{
			running = true;
			SharedPreferences preferences = getSharedPreferences(PREF_FILE_NAME, MODE_PRIVATE);		
			SharedPreferences.Editor editor = preferences.edit();
			editor.putBoolean("running", running);			
			editor.commit();
			_ROOTPATH = preferences.getString("ROOTPATH", "");
			_SENSORDATA = preferences.getString("SENSORDATA", "");
			
			if(_ROOTPATH.equals("") || _SENSORDATA.equals(""))
				stopSelf();
			
			Application._Context = this.getApplicationContext();
			Application._Wockets = new Wockets();
			Application._Controller= new WocketsController();
			WocketsService._Context=Application._Context;
			WocketsService._Controller=Application._Controller;											
			Application._Controller.Initialize(_SENSORDATA);
			
			String dataStoragePath = Application._Controller._DataStoragePath;
			//Log.d(TAG,"DataStoragePath:" + dataStoragePath);
			editor.putString("DATASTORAGEPATH",dataStoragePath);
			editor.commit();
			
//			String dataPath = preferences.getString("DATASTORAGEPATH", "");
//			if(!dataPath.equals(""))
//			{
//				Application._Controller._DataStoragePath = dataPath;
//			}
			
			Application._running = true;			
			Application._Controller.FromXML(this._ROOTPATH + _SENSORDATA);
			SensorList sensorList = Application._Controller._Sensors;
			BluetoothAdapter.getDefaultAdapter().enable();
			for(int i = 0 ; i < sensorList.size(); i++)
			{
				Application._Controller._Sensors.get(i).Initialize();

				try 
				{
					Thread.sleep(1000);
				}
				catch (InterruptedException e) 
				{					
					String strMsg = TAG + ":startSession()\n";
					strMsg += e.getMessage() + "\n";
					
					StringWriter errTrace = new StringWriter();
					e.printStackTrace(new PrintWriter(errTrace));
					
					strMsg += errTrace.toString();
					
				//	Logger.error(strMsg);
				}
				
				if(sensorList.get(i)._Class.equalsIgnoreCase("Wockets"))
					Application._Wockets.add(new Wocket(((RFCOMMReceiver)sensorList.get(i)._Receiver)._Address, sensorList.get(i)._ID));
					
			}
			
			if (Application._Wockets.size()>0)
			{
				if(Application._Controller != null)
					Application._Controller.Connect(Application._Wockets.toAddressArrayList());
			}
				
			 else
			 {
				 Toast.makeText(this, "Unable to connect to wockets", Toast.LENGTH_LONG).show();
				 return;
			 }		 
		        
			
		}
	}	

}
