package edu.neu.madcourse.pratikdhandharia;

import java.util.ArrayList;
import java.util.HashMap;

public class User {
	public String username;
	public String password;
	HashMap<Integer, Integer> scores;
	public int totalChallenges;
	public int totalWon;	
	public int current_score;
	public int current_level;
	public int booster;
	ArrayList<Opponent> challenges;
	ArrayList<Opponent> challenged;
	
	public User(){
		username = new String("");
		password = new String("");
		current_score = 0;
		totalChallenges = 0;
		totalWon = 0;
		booster = 1;
		current_level = 1;
		scores = new HashMap<Integer, Integer>(15, 0.8f);
		challenged = new ArrayList<Opponent>();
		challenges = new ArrayList<Opponent>();
	}
	
	public User(String usr, String pwd){
		username = usr;
		password = pwd;
		current_score = 0;
		totalChallenges = 0;
		totalWon = 0;
		booster = 1;
		current_level = 1;
		scores = new HashMap<Integer, Integer>(15, 0.8f);
		challenged = new ArrayList<Opponent>();
		challenges = new ArrayList<Opponent>();
	}
	
	public void addScoreToLevel(int level, int score) {
		if (level > 0 && level <= Ngage.listOfLevels.levels.size())
			scores.put(level, score);
	}
	
	public boolean addChallenged(Opponent opp)
	{
		if (challenged.size() < 1)
		{
			challenged.add(opp);
			return true;
		}
		else
			return false;
	}
	
	public boolean addChallenges(Opponent opp)
	{
		if (challenges.size() < 1)
		{
			challenges.add(opp);
			return true;
		}
		else
			return false;
	}
	
	public void deleteChallenged()
	{
		if (challenged.size() != 0)
			challenged.clear();		
	}
	
	public void deleteChallenges()
	{
		if (challenges.size() != 0)
			challenges.clear();		
	}
}
