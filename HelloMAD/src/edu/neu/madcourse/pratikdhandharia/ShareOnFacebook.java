package edu.neu.madcourse.pratikdhandharia;

import edu.neu.madcourse.pratikdhandharia.Facebook.DialogListener;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

public class ShareOnFacebook extends Activity implements OnClickListener{
	
	private static final String APP_ID = "520189357991815";
	private static final String[] PERMISSIONS = new String[] {"publish_stream"};

	private static final String TOKEN = "access_token";
    private static final String EXPIRES = "expires_in";
    private static final String KEY = "facebook-credentials";

	private Facebook facebook;
	private String messageToPost;
	private Button share, donotshare;
	private EditText edittext;
	private int block_width;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.shareonfacebook);
		
		Display display = getWindowManager().getDefaultDisplay();
		block_width = display.getWidth() / 2 ;
		
		share = (Button) findViewById(R.id.FacebookShareButton);
		share.setOnClickListener(this);
		share.setWidth((int) (block_width *0.8));
		share.setBackgroundResource(R.drawable.button);
		share.setTextColor(Color.WHITE);
		
		donotshare = (Button) findViewById(R.id.FacebookShareNotButton);
		donotshare.setOnClickListener(this);
		donotshare.setWidth((int) (block_width * 0.8));
		donotshare.setBackgroundResource(R.drawable.button);
		donotshare.setTextColor(Color.WHITE);
		
		facebook = new Facebook(APP_ID);
		edittext = (EditText) findViewById(R.id.txtEdittext);
		edittext.setWidth((int) (block_width*0.8));
        Animation a = AnimationUtils.loadAnimation(this, R.anim.fadein);
        a.reset();
        LinearLayout ll = (LinearLayout) findViewById(R.id.sharefacebookview);
        ll.clearAnimation();
        ll.startAnimation(a);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.FacebookShareButton:
			messageToPost = edittext.getEditableText().toString();
			if(messageToPost.equals(""))
				messageToPost = "Did not enter text in the text box!!";
			if(facebook.isSessionValid())
			{
				postToWall(messageToPost);
			}
			else
			{
			 facebook.authorize(this, PERMISSIONS, Facebook.FORCE_DIALOG_AUTH, new LoginDialogListener());
			}
			break;
		case R.id.FacebookShareNotButton:
			finish();
			break;
		}
	}
	
	public boolean saveCredentials(Facebook facebook) {
    	Editor editor = getApplicationContext().getSharedPreferences(KEY, Context.MODE_PRIVATE).edit();
    	editor.putString(TOKEN, facebook.getAccessToken());
    	editor.putLong(EXPIRES, facebook.getAccessExpires());
    	return editor.commit();
	}

	public boolean restoreCredentials(Facebook facebook) {
    	SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(KEY, Context.MODE_PRIVATE);
    	facebook.setAccessToken(sharedPreferences.getString(TOKEN, null));
    	facebook.setAccessExpires(sharedPreferences.getLong(EXPIRES, 0));
    	return facebook.isSessionValid();
	}

	
	public void postToWall(String message){
		Bundle parameters = new Bundle();
                parameters.putString("message", message);
                parameters.putString("description", "topic share");
                try {
        	        facebook.request("me");
			String response = facebook.request("me/feed", parameters, "POST");
			Log.d("Tests", "got response: " + response);
			if (response == null || response.equals("") ||
			        response.equals("false")) {
				showToast("Blank response.");
			}
			else {
				showToast("Message posted to your facebook wall!");
			}
			finish();
		} catch (Exception e) {
			showToast("Failed to post to wall!");
			e.printStackTrace();
			finish();
		}
	}
	
	class LoginDialogListener implements DialogListener {
	    public void onComplete(Bundle values) {
	    	saveCredentials(facebook);
	    	if (messageToPost != null){
			postToWall(messageToPost);
		}
	    }
	    public void onFacebookError(FacebookError error) {
	    	showToast("Authentication with Facebook failed!");
	        finish();
	    }
	    public void onError(DialogError error) {
	    	showToast("Authentication with Facebook failed!");
	        finish();
	    }
	    public void onCancel() {
	    	showToast("Authentication with Facebook cancelled!");
	        finish();
	    }
	}

	private void showToast(String message){
		Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
	}

}
