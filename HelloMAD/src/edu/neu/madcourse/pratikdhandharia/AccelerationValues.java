package edu.neu.madcourse.pratikdhandharia;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.everyfit.wockets.data.AccelerationData;
import com.everyfit.wockets.logging.Logger;
import com.everyfit.wockets.sensors.Sensor;

public class AccelerationValues extends Activity implements
		DataServiceInterface,OnClickListener {
	
	TextView txtsample;
	Button btnstop;
	Intent i;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.collectdata);
		//CopyOfCollectData.addListener(this);
//		i = new Intent(this, CopyOfCollectData.class);
//		startService(i);
		txtsample = (TextView) findViewById(R.id.txtsample);
		btnstop = (Button) findViewById(R.id.btnstop);
		btnstop.setOnClickListener(this);
	}

	public void notifyAccel(Sensor s, long time, AccelerationData datum) {
		// TODO Auto-generated method stub

		Log.d("Wocket", "These are the values that you need, for sensor id="
				+ s._ID + ", X=" + datum._X + ", Y=" + datum._Y + ", Z="
				+ datum._Z);
		txtsample.setText("These are the values that you need, for sensor id="
				+ s._ID + ", X=" + datum._X + ", Y=" + datum._Y + ", Z="
				+ datum._Z);

		Logger.log(s._ID, datum._X + "," + datum._Y + "," + datum._Z);
	}


	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.btnstop:
			//stopService(i);
			//CopyOfCollectData.removeListener(this);
			Log.d("wocket", "stopped service");
			break;
		}
	}

}
