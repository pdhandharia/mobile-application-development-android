package edu.neu.madcourse.pratikdhandharia;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class NgageInstruction extends Activity implements OnClickListener{
	
	Button move1, move2,move3,move4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ngageinstruction);
		
		Display display = getWindowManager().getDefaultDisplay();
		int width = (int)( display.getWidth() *0.7);
		
		move1 = (Button) findViewById(R.id.btnmove1);
		move1.setOnClickListener(this);
		move1.setBackgroundResource(R.drawable.button);
		move1.setTextColor(Color.WHITE);
		move1.setWidth(width);
		
		move2 = (Button) findViewById(R.id.btnmove2);
		move2.setOnClickListener(this);
		move2.setBackgroundResource(R.drawable.button);
		move2.setTextColor(Color.WHITE);
		move2.setWidth(width);
		
		move3 = (Button) findViewById(R.id.btnmove3);
		move3.setOnClickListener(this);
		move3.setBackgroundResource(R.drawable.button);
		move3.setTextColor(Color.WHITE);
		move3.setWidth(width);
		
		move4 = (Button) findViewById(R.id.btnmove4);
		move4.setOnClickListener(this);
		move4.setBackgroundResource(R.drawable.button);
		move4.setTextColor(Color.WHITE);
		move4.setWidth(width);
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch(v.getId())
		{
		case R.id.btnmove1:
			Intent i1 = new Intent(this,Move1Instructions.class);
			startActivity(i1);
			break;
		case R.id.btnmove2:
			Intent i2 = new Intent(this,Move2Instructions.class);
			startActivity(i2);
			break;
		case R.id.btnmove3:
			Intent i3 = new Intent(this,Move3Instructions.class);
			startActivity(i3);
			break;
		case R.id.btnmove4:
			Intent i4 = new Intent(this,Move4Instructions.class);
			startActivity(i4);
			break;
		}
		
	}

}
