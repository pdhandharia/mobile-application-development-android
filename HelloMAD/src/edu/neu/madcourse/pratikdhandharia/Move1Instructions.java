package edu.neu.madcourse.pratikdhandharia;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.Display;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TableLayout.LayoutParams;

public class Move1Instructions extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.move1instruction);
		
		Display display = getWindowManager().getDefaultDisplay();
		int width = (int)( display.getWidth() *0.9);
		
			ImageView img = (ImageView)findViewById(R.id.imgMove1);
		 //img.setBackgroundResource(R.drawable);
			img.setBackgroundResource(R.drawable.move1animation);

		 // Get the background, which has been compiled to an AnimationDrawable object.
		 AnimationDrawable frameAnimation = (AnimationDrawable) img.getBackground();

		 // Start the animation (looped playback by default).
		 frameAnimation.start();
		
		TextView t = (TextView) findViewById(R.id.txtMove1Desc);
		t.setTextColor(Color.BLACK);
		t.setWidth(width);
 	}
	

}
