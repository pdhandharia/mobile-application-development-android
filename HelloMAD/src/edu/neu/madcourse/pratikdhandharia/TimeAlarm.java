package edu.neu.madcourse.pratikdhandharia;

import edu.neu.mobileclass.apis.KeyValueAPI;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.PowerManager;
import android.util.Log;
import android.widget.Toast;

public class TimeAlarm extends BroadcastReceiver{
	NotificationManager nm;
	 MediaPlayer mp;
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "");
        wl.acquire();
        if (NgageMainActivity.user != null && ! NgageMainActivity.user.username.equals(""))
        {
        	if (Ngage.dataNotCleared && KeyValueAPI.isServerAvailable())
        	{
        		clearKeys();
        		Ngage.dataNotCleared = false;
        	}
        	if (KeyValueAPI.isServerAvailable())
        	{
        		String opponent = KeyValueAPI.get("Prachi_Pratik", "neu12345", NgageMainActivity.user.username);
        		if((! opponent.equals("")) && (! opponent.contains("ERROR")) && (Ngage.notification))
        		{
        			mp = MediaPlayer.create(context, R.raw.gridclick);
        			mp.start();
        			nm = (NotificationManager) context
        					.getSystemService(Context.NOTIFICATION_SERVICE);
        			Intent i = new Intent(context, ChallengeRequest.class);
        			i.putExtra("OPPONENT", opponent);
        			PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
        					i, 0);
        			Notification notif = new Notification(R.drawable.close,
        					"N-Gage Challenge", System.currentTimeMillis());
        			notif.setLatestEventInfo(context, "N-gage", "You are challenged by "+opponent, contentIntent);
        			notif.flags |= Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_AUTO_CANCEL;
        			nm.notify(1, notif);
        		}
        	}
        	if (KeyValueAPI.isServerAvailable())
        	{
        		String challengeScore = KeyValueAPI.get("Prachi_Pratik", "neu12345", NgageMainActivity.user.username+"_challengeScore");
        		if (! challengeScore.equals("") && ! challengeScore.contains("ERROR"))
        		{
        			if (Integer.parseInt(challengeScore) == -1)
        			{
        				if (KeyValueAPI.isServerAvailable())
        					clearKeys();
        				else
        					Ngage.dataNotCleared = true;
        				mp = MediaPlayer.create(context, R.raw.gridclick);
        				mp.start();
        				nm = (NotificationManager) context
        						.getSystemService(Context.NOTIFICATION_SERVICE);
        				Intent i = new Intent(context, ChallengeMain.class);
        				PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
        						i, 0);
        				Notification notif = new Notification(R.drawable.close,
        						"N-Gage Challenge", System.currentTimeMillis());
        				notif.setLatestEventInfo(context, "N-gage", "Your Challenge has been declined.", contentIntent);
        				notif.flags |= Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_AUTO_CANCEL;
        				nm.notify(1, notif);
        			}
        			else
        			{
        				try
        				{
        					
	        				NgageMainActivity.user.totalChallenges++;
//	        				ChallengeMain cm = new ChallengeMain();
//	        				cm.saveTotalChallenges(NgageMainActivity.user.totalChallenges);
	        				Log.d("error",""+ NgageMainActivity.user.challenged.get(0).score);
	        				Log.d("error 1", "" + Integer.parseInt(challengeScore));
	        				String msg;
	        				//int level = NgageMainActivity.user.challenged.get(0).level;
	        				if (Integer.parseInt(challengeScore) < NgageMainActivity.user.challenged.get(0).score)
	        				{
	        					NgageMainActivity.user.totalWon++;
	        					//cm.saveChallengesWon(NgageMainActivity.user.totalWon);
	        					msg = "You won the challenge.";
	        					NgageMainActivity.user.booster += 6;
	        				}
	        				else
	        				{
	        					msg = "You lost the challenge.";
	        				}
	        				mp = MediaPlayer.create(context, R.raw.gridclick);
	        				mp.start();
	        				nm = (NotificationManager) context
	        						.getSystemService(Context.NOTIFICATION_SERVICE);
	        				Intent i = new Intent(context, ChallengeMain.class);
	        				PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
	        						i, 0);
	        				Notification notif = new Notification(R.drawable.close,
	        						"N-Gage Challenge", System.currentTimeMillis());
	        				notif.setLatestEventInfo(context, "N-gage", msg, contentIntent);
	        				notif.flags |= Notification.FLAG_ONLY_ALERT_ONCE | Notification.FLAG_AUTO_CANCEL;
	        				nm.notify(1, notif);
	        				clearKeys();
        				}
        				catch(Exception e)
        				{
        					Log.d("Error", e.getMessage());
        				}
        			}
        		}
        	}
        }
        wl.release();
	}
	private void clearKeys() {
		// TODO Auto-generated method stub
			Opponent opp = NgageMainActivity.user.challenged.get(0);
			if(KeyValueAPI.isServerAvailable())
			{
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", NgageMainActivity.user.username+"_challengeScore"); 
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", NgageMainActivity.user.username+"_challengeScore");
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", opp.username+"_level");
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", opp.username+"_score");
			KeyValueAPI.clearKey("Prachi_Pratik", "neu12345", opp.username);
			NgageMainActivity.user.deleteChallenged();
			}
			else Ngage.dataNotCleared = true;
			
	}
}
