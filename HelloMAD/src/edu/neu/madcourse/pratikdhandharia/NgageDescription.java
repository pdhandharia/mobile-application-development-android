package edu.neu.madcourse.pratikdhandharia;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NgageDescription extends Activity {
	Button btnplay;
	TextView txttitle,txtdesc;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.ngagedescription);
		
		Display display = getWindowManager().getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();
		
		txttitle = (TextView) findViewById(R.id.txtAppName);
		txtdesc = (TextView) findViewById(R.id.ngageDescription);
		
		txttitle.setTextColor(Color.BLACK);
		txtdesc.setTextColor(Color.BLACK);
		
		btnplay = (Button) findViewById(R.id.btnstartNgage);
		btnplay.setWidth((int) (width*0.7));
		btnplay.setTextColor(Color.WHITE);
		btnplay.setHeight(((int) (height*0.1)));
		
		btnplay.setOnClickListener(new View.OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(),NgageMainActivity.class);
				startActivity(i);
			}
		});
		
	}

}
