package edu.neu.madcourse.pratikdhandharia;

import java.io.BufferedReader;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

public class BoggleGame extends Activity implements OnClickListener {
	private static final String TAG = "BoggleGame";
	private static final String PREF_BOGGLE = "boggle";
	private static final String PREF_TIMER = "0";
	private static final String PREF_CURRENTWORD = "";
	private static final String PREF_WORDLIST = "";
	private static final String PREF_WORDLISTINDEX = "";
	private static final String PREF_SCORE = "";

	public static final String KEY = "1";
	
	MediaPlayer mp_letter,mp_successful;
	AudioManager am;

	public static final Bundle wordlist_data = new Bundle();
	public static final Bundle wordlistindex_data = new Bundle();
	public static final Bundle score_data = new Bundle();

	public static final int CONTINUE = 0;
	public static final int NEW_GAME = 1;

	private String boggle;
	private ArrayList<String> boggleWords;
	public ArrayList<Integer> bogglewordsindex;
	public String currentWord;
	public int score;

	private long pauseTimer = 0;
	private int startTimer = 60000;
	private int timeInterval = 1000;
	private Mycountdowntimer mycountdown;

	private BoggleView boggleview;

	LayoutParams l;
	TableLayout boggletable;
	TextView txtScore, txtTimer, txtCurrentWord;
	Button btnbogglegamepause, btnbogglecheckword, btnlistofwords;
	TableRow bogglegridrow;

	int height, width;
	int grid_start;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.bogglegame);

		int key = getIntent().getIntExtra(KEY, NEW_GAME);

		boggle = getBoggle(key);

		// populatedictionary();
		mp_letter = MediaPlayer.create(this, R.raw.letter_click);
		mp_successful = MediaPlayer.create(this,R.raw.successful_word);
		am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

		boggletable = (TableLayout) findViewById(R.id.boggletablelayout);
		bogglegridrow = new TableRow(this);

		txtScore = (TextView) findViewById(R.id.txtboggleScore);
		txtTimer = (TextView) findViewById(R.id.txttimer);
		txtCurrentWord = (TextView) findViewById(R.id.txtCurrentWord);
		btnbogglegamepause = (Button) findViewById(R.id.btnbogglePause);
		btnbogglecheckword = (Button) findViewById(R.id.btncheckword);
		btnlistofwords = (Button) findViewById(R.id.btnbogglewords);

		// on click for pause button
		btnbogglegamepause.setOnClickListener(this);

		btnbogglecheckword.setOnClickListener(this);

		btnlistofwords.setOnClickListener(this);

		Display display = getWindowManager().getDefaultDisplay();
		height = display.getHeight();
		width = display.getWidth();
		grid_start = (int) (height * 0.3);

		txtTimer.setWidth(width / 2);
		txtTimer.setHeight(grid_start / 3);
		txtScore.setWidth(width / 2);
		txtScore.setHeight(grid_start / 3);
		txtCurrentWord.setWidth(width / 2);
		txtCurrentWord.setHeight(grid_start / 3);
		txtCurrentWord.setText(currentWord);

		txtScore.setText("Your Score : " + String.valueOf(score));

		btnbogglegamepause.setText("Pause");
		btnbogglecheckword.setText("Check");
		btnlistofwords.setText("List Of Words");
		l = new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT);
		boggleview = new BoggleView(this);

		TableRow.LayoutParams boggleviewlayout = new TableRow.LayoutParams();

		boggleviewlayout.span = 2;
		boggleview.setLayoutParams(boggleviewlayout);

		bogglegridrow.addView(boggleview);

		// boggleview.setLayoutParams()
		boggletable.addView(bogglegridrow, l);
		boggleview.requestFocus();
		Log.d(TAG, "on create called");
	}



	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		btnpausehandler();
		finish();
		// Log.d(TAG,"" + String.valueOf(boggleWords.size()));
	}
	
	private String getBoggle(int key) {
		// TODO Auto-generated method stub

		String s = "";
		switch (key) {
		case CONTINUE:
			s = btnbogglecontinuehandler();
			break;
		case NEW_GAME:
			s = btnbogglenewgamehandler();
			break;
		}
		return s;
	}

	// handler for boggle continue button
	public String btnbogglecontinuehandler() {
		String s = "";
		s = getPreferences(MODE_PRIVATE).getString(PREF_BOGGLE, getNewBoggle());
		pauseTimer = getPreferences(MODE_PRIVATE).getLong(PREF_TIMER,
				startTimer);
		mycountdown = new Mycountdowntimer(pauseTimer, timeInterval);
		mycountdown.start();
		currentWord = getPreferences(MODE_PRIVATE).getString(PREF_CURRENTWORD,
				"");
		boggleWords = wordlist_data.getStringArrayList(PREF_WORDLIST);
		if (boggleWords == null)
			boggleWords = new ArrayList<String>();
		bogglewordsindex = wordlistindex_data
				.getIntegerArrayList(PREF_WORDLISTINDEX);
		if (bogglewordsindex == null)
			bogglewordsindex = new ArrayList<Integer>();
		score = score_data.getInt(PREF_SCORE, 0);
		return s;
	}

	// handler for boggle new game button
	public String btnbogglenewgamehandler() {
		String s = "";
		s = getNewBoggle();
		currentWord = "";
		mycountdown = new Mycountdowntimer(startTimer, timeInterval);
		mycountdown.start();
		boggleWords = new ArrayList<String>();
		bogglewordsindex = new ArrayList<Integer>();
		score = 0;
		return s;
	}

	// function to generate new boggle board
	private String getNewBoggle() {
		String s = "";
		String game[] = { "aaeegn", "elrtty", "aoottw", "abbjoo", "ehrtvw",
				"cimotv", "distty", "eiosst", "delrvy", "achops", "himnqu",
				"eeinsu", "eeghnw", "affkps", "hlnnrz", "deilrx" };
		int random = 0;
		for (int i = 0; i < game.length; i++) {
			random = (int) ((Math.random() * 10 + 1) % 6);
			s += game[i].substring(random, random + 1);
		}
		return s;
	}

	// function to get a character at particular location
	public String getStringAt(int i) {
		return boggle.substring(i, i + 1);
	}

	// function to check if the current letter is already clicked
	public Boolean isalreadyclicked(int i) {
		if (bogglewordsindex.size() > 0)
			return bogglewordsindex.contains(i);
		else
			return false;
	}
	
	public int getlastclicked()
	{
		if (bogglewordsindex.size() <=0)
			return -1;
		return bogglewordsindex.get(bogglewordsindex.size()-1);
	}
	
	public void handlefinishtime()
	{
		txtTimer.setText("Time Over");
		mycountdown.cancel();
		AlertDialog ad = new AlertDialog.Builder(this).create();
		ad.setTitle("Your Score: " + score);
		String s = "";
		for (int i = 0; i < boggleWords.size(); i++) {
			s += boggleWords.get(i);
			if(i != boggleWords.size() - 1)
			{
				s += "-";
			}
		}
		ad.setMessage(s);
		ad.setCancelable(false);
		ad.setButton("Finish!!", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				boggle = getNewBoggle();
				currentWord = "";
				pauseTimer = startTimer;
				boggleWords = new ArrayList<String>();
				bogglewordsindex = new ArrayList<Integer>();
				score = 0;
				finish();
			}
		});
		ad.show();
	}

	// Count down timer class
	public class Mycountdowntimer extends CountDownTimer {

		public Mycountdowntimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			Log.d(TAG, "pause timer value: " + String.valueOf(pauseTimer));
				handlefinishtime();
		}

		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub
			pauseTimer = millisUntilFinished;
			txtTimer.setText("Time Remaining: "
					+ String.valueOf(millisUntilFinished / 1000));
		}
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnbogglePause:
			btnpausehandler();
			finish();
			break;
		case R.id.btncheckword:
			btncheckwordhandler();
			break;
		case R.id.btnbogglewords:
			btnlistofWordshandler();
			break;
		}
	}

	// handler for button list of words
	public void btnlistofWordshandler() {
		AlertDialog ad = new AlertDialog.Builder(this).create();
		String s = "";
		for (int i = 0; i < boggleWords.size(); i++) {
			s += boggleWords.get(i);
			if(i != boggleWords.size() - 1)
			{
				s += "-";
			}
		}
		ad.setMessage(s);
		ad.show();
	}

	// handler for button pause
	public void btnpausehandler() {
		getPreferences(MODE_PRIVATE).edit().putString(PREF_BOGGLE, boggle)
				.putLong(PREF_TIMER, pauseTimer)
				.putString(PREF_CURRENTWORD, currentWord).commit();

		wordlist_data.putStringArrayList(PREF_WORDLIST, boggleWords);
		wordlistindex_data.putIntegerArrayList(PREF_WORDLISTINDEX,
				bogglewordsindex);
		score_data.putInt(PREF_SCORE, score);
		mycountdown.cancel();
	}

	// handler for btn check
	public void btncheckwordhandler() {
		// TODO check for blank word, invalid word, same word
		// TODO also add logic for calculating points.
		if (currentWord.length() > 0) {
			if (boggleWords.contains(currentWord)) {
				Toast duplicate = Toast.makeText(this, "duplicate word",
						Toast.LENGTH_SHORT);
				duplicate.show();
			} else {
				if (isvalidWord()) {
					calculateScore(currentWord);

					// Adding word to user's word list.
					boggleWords.add(currentWord);

					// updating the score textview
					txtScore.setText("Your Score: " + String.valueOf(score));
					
					mp_successful.start();
					
				}

			}
			// resetting the Current word to blank
			currentWord = "";
			txtCurrentWord.setText("");

			// Reseting the selected word list
			bogglewordsindex = new ArrayList<Integer>();
		}
	}


	public boolean isvalidWord() {
		boolean result = false;
		try {
			String str = "";
			int len = currentWord.length();
			char firstChar = currentWord.charAt(0);
			InputStream is = this.getAssets().open(String.valueOf(len) + "/" + firstChar + ".txt");
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			while((str = br.readLine()) != null)
			{
				if (str.trim().equals(currentWord))
				{
					result = true;
					break;
				}
			}
			br.close();
			is.close();
		} catch (Exception e) {
			Log.d(TAG, "reading from file: " + e.getMessage());
		}
		return result;
	}

	// calculate and update the score
	public void calculateScore(String currentword) {
		if (currentword.length() >= 3)
			score += currentword.length();
	}


}
