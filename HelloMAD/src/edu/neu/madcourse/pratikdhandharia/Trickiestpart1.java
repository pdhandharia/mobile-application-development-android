package edu.neu.madcourse.pratikdhandharia;

import edu.neu.madcourse.pratikdhandharia.Trickiestpart2.Mycountdowntimer;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.view.Menu;
import android.widget.TextView;

public class Trickiestpart1 extends Activity implements SensorEventListener{

	private String TAG = "Accelerometer";
	final Context context = this;
	private SensorManager mSensorManager;
	private Sensor mAccelerometer;
	private Mycountdowntimer mycountdowntimer;
	private TextView x_axis, y_axis, z_axis,txtTimer,value;
	private float x,y,z;
	private MediaPlayer mp;
	private mydata my;
	private final static int SIZE = 25;
	private final static int THRESHOLD = 50;
	private int score;
	private final static int LEVEL1_X_MIN = -50,LEVEL1_X_MAX=75;
	private final static int LEVEL1_Y_MIN = -150,LEVEL1_Y_MAX=200;
	private final static int LEVEL1_Z_MIN = -200,LEVEL1_Z_MAX=100;
	public AlertDialog ad;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try
        {
            setContentView(R.layout.trickiestpartlayout);
            mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            x_axis = (TextView) findViewById(R.id.x_axis);
            y_axis = (TextView) findViewById(R.id.y_axis);
            z_axis = (TextView) findViewById(R.id.z_axis); 
            txtTimer = (TextView) findViewById(R.id.txtTimer);
            value = (TextView) findViewById(R.id.value);
            score = 0;
            txtTimer.setText("Timer: ");
            mp = MediaPlayer.create(this, R.raw.gridclick);
            my = new mydata();
        
        ad = new AlertDialog.Builder(this).create();
        ad.setMessage("Move the phone vertically facing outwards");
        ad.setTitle("Instructions");
        ad.setButton("Ok", new DialogInterface.OnClickListener() {
			
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
			startgame();
			}
		});
        ad.show();
        
        }
        catch(Exception e)
        {
        	Log.d(TAG, e.getMessage());
        }
    }
	
	public void startgame()
	{		        
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
	    mycountdowntimer = new Mycountdowntimer(20000, 100);
	    mycountdowntimer.start();	
	    ad.dismiss();
    }
	
	public void onSensorChanged(SensorEvent event) {
		// TODO Auto-generated method stub
		//Log.d("acceleration", String.valueOf(System.currentTimeMillis()));
		
		x = event.values[0]*10;
		y = event.values[1]*10;
		z = event.values[2]*10;
		
		x_axis.setText("X: " + x);
		y_axis.setText("Y: " + y);
		z_axis.setText("Z: " + z);
		
		Log.d(TAG, x + "," + y + "," +z); 
		//Log.d(TAG + "1",String.valueOf((Math.sqrt((event.values[0]*event.values[0]) + (event.values[1]*event.values[1]) + (event.values[2]*event.values[2])) - 9.8)));
		my.add(x, y, z);
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		unregisterAccelerometer();
		//mp.stop();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }
	
	public void unregisterAccelerometer()
	{
		mSensorManager.unregisterListener(this);
	}


	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	// can be safely ignored for this demo
	}
	
	public class Mycountdowntimer extends CountDownTimer {

		public Mycountdowntimer(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			try
			{
			unregisterAccelerometer();
			mp.stop();
			}
			catch(Exception e)
			{
				Log.d(TAG, e.getMessage());
			}
		}

		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub
			txtTimer.setText("Timer: " + millisUntilFinished/1000);
			
			if (my.validate())
			{
				mp.start();
				mp.setVolume(1000, 1000);
				score++;
				value.setText("Score: " + String.valueOf(score));
			}
		}
	}
	
	private class mydata {
		float[] x = new float[SIZE];
		float[] y = new float[SIZE];
		float[] z = new float[SIZE];
		mydata()
		{
			for(int i =0;i<SIZE;i++)
				{x[i] = 0;y[i] = 0;z[i] = 0;}
		}
		
		public void add(float last_x,float last_y,float last_z)
		{
			int i;
			for(i = 0;i<SIZE-1;i++)
			{
				x[i] = x[i+1];
				y[i] = y[i+1];
				z[i] = z[i+1];
			}
			x[i] = last_x;
			y[i] = last_y;
			z[i] = last_z;
		}
		
		public boolean validate()
		{
			float max_x=0,max_y=0,max_z=0;
			float min_x=999,min_y=999,min_z=999;

			
			for(int i =0;i<SIZE;i++)
			{
//				if(x[i]>max_x)
//					max_x = x[i];
//				if(x[i] < min_x)
//					min_x = x[i];
				
				if(y[i]>max_y)
					max_y = y[i];
				if(y[i] < min_y)
					min_y = y[i];
				
//				if(z[i]>max_z)
//					max_z = z[i];
//				if(z[i] < min_z)
//					min_z = z[i];
			}
			return isValid(min_x,max_x, min_y,max_y,min_z,max_z);
//			return (mean_x/10 + mean_y/10 + mean_z/10)/3;
		}
		public boolean isValid(float min_x,float max_x,float min_y,float max_y,float min_z, float max_z)
		{
			boolean result = false;
			result = LEVEL1_Y_MIN - THRESHOLD < min_y && min_y < LEVEL1_Y_MIN + THRESHOLD &&
					 LEVEL1_Y_MAX - THRESHOLD < max_y && max_y < LEVEL1_Y_MAX + THRESHOLD; 
					 
//					 LEVEL1_X_MIN - THRESHOLD < min_x && min_x < LEVEL1_X_MIN + THRESHOLD &&
//					 LEVEL1_X_MAX - THRESHOLD < max_x && max_x < LEVEL1_X_MAX + THRESHOLD &&
//					 
//					 LEVEL1_Z_MIN - THRESHOLD < min_z && min_z < LEVEL1_Z_MIN + THRESHOLD &&
//					 LEVEL1_Z_MAX - THRESHOLD < max_z && max_z < LEVEL1_Z_MAX + THRESHOLD;
			Log.d(TAG, String.valueOf(result));
			return result;
		}
	}
	
}
