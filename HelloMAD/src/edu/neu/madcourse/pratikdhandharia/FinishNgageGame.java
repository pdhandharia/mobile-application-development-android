package edu.neu.madcourse.pratikdhandharia;

import edu.neu.mobileclass.apis.KeyValueAPI;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class FinishNgageGame extends Activity implements OnClickListener {

	private static final int ShowToast = 1;
	private static final int ShowFinishChallengeDialog = 2;
	private static final int ShowOnlineUsers = 3;
	private static final String MESSAGE = "message";

	int block_height, block_width;
	Boolean challengeRequest = false;
	TextView userHighScore, onlineHighScore, levelNumber, currentScore;
	Button btnshare, btnreplay, btnnext, btnchallenge, btnquit;
	static public AlarmManager am;
	final Context context = this;
	private String online_high_score;
	LoadData ld;
	loadDataforChallenge ldc;
	HandleChallengeTask hct;
	AlertDialog a;
	Handler handler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.finish_game);
		
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		a = new AlertDialog.Builder(this).create();
		handler = new Handler() {

			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
				super.handleMessage(msg);
				switch (msg.what) {
				case ShowToast:
					Toast.makeText(getApplicationContext(), msg.getData()
							.getString(MESSAGE), Toast.LENGTH_LONG);
					break;
				case ShowFinishChallengeDialog:
					a.setTitle("Game Over");
					a.setMessage(msg.getData().getString(MESSAGE));
					a.setButton("OK", new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
						}
					});
					a.show();
					break;
				case ShowOnlineUsers:
					final String[] m_users = msg.getData().getStringArray(
							MESSAGE);

					AlertDialog.Builder b = new Builder(FinishNgageGame.this);
					b.setTitle("Available users");
					b.setItems(m_users, new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							dialog.dismiss();
							handleChallenger(m_users, which);
						}
					});
					b.show();
					break;
				}
			}

		};

		ld = new LoadData(this);
		ld.execute();

		
		//ldc = new loadDataforChallenge();
	}

	public void initializeGame() {
		challengeRequest = getIntent().getBooleanExtra("CHALLENGE", false);

		Log.d("challengerequest", String.valueOf(challengeRequest));
		if (challengeRequest)
		{
			hct = new HandleChallengeTask(this);
			hct.execute();
		}
		Display display = getWindowManager().getDefaultDisplay();
		block_height = display.getHeight() / 8;
		block_width = display.getWidth() / 2;

		userHighScore = (TextView) findViewById(R.id.txtuserhighscore);
		onlineHighScore = (TextView) findViewById(R.id.txtonlinehighscore);
		levelNumber = (TextView) findViewById(R.id.txtlevelnumber);
		currentScore = (TextView) findViewById(R.id.txtcurrentscore);

		btnchallenge = (Button) findViewById(R.id.btnchallenge);
		btnchallenge.setOnClickListener(this);
		btnnext = (Button) findViewById(R.id.btnnext);
		btnnext.setOnClickListener(this);
		btnreplay = (Button) findViewById(R.id.btnreplay);
		btnreplay.setOnClickListener(this);
		btnshare = (Button) findViewById(R.id.btnShare);
		btnshare.setOnClickListener(this);
		btnquit = (Button) findViewById(R.id.btnfinishgamequit);
		btnquit.setOnClickListener(this);
		
		userHighScore.setWidth(block_width);
		onlineHighScore.setWidth(block_width);
		levelNumber.setWidth(block_width * 2);
		currentScore.setWidth(block_width * 2);
		btnchallenge.setWidth(block_width);
		btnnext.setWidth(block_width);
		btnreplay.setWidth(block_width);
		btnshare.setWidth(block_width);
		btnquit.setWidth(block_width);

		userHighScore.setHeight(block_height);
		onlineHighScore.setHeight(block_height);
		levelNumber.setHeight(block_height);
		currentScore.setHeight(block_height);
		btnchallenge.setHeight((int) (block_height*0.8));
		btnnext.setHeight((int) (block_height*0.8));
		btnreplay.setHeight((int) (block_height*0.8));
		btnshare.setHeight((int) (block_height*0.8));
		btnquit.setHeight((int) (block_height*0.8));

		btnchallenge.setBackgroundResource(R.drawable.button);
		btnnext.setBackgroundResource(R.drawable.button);
		btnreplay.setBackgroundResource(R.drawable.button);
		btnshare.setBackgroundResource(R.drawable.button);
		btnquit.setBackgroundResource(R.drawable.button);
		
		btnchallenge.setTextColor(Color.WHITE);
		btnnext.setTextColor(Color.WHITE);
		btnreplay.setTextColor(Color.WHITE);
		btnshare.setTextColor(Color.WHITE);
		btnquit.setTextColor(Color.WHITE);
		
		userHighScore.setTextColor(Color.BLACK);
		onlineHighScore.setTextColor(Color.BLACK);
		levelNumber.setTextColor(Color.BLACK);
		currentScore.setTextColor(Color.BLACK);

		Integer yourhighscore = NgageMainActivity.user.scores
				.get(NgageMainActivity.user.current_level);
		if (yourhighscore == null)
			userHighScore
					.setText("Your High Score : You haven't played this level yet");
		else
			userHighScore.setText("Your High Score : " + yourhighscore);

		onlineHighScore.setText(online_high_score);

		levelNumber.setText("Level " + NgageMainActivity.user.current_level);
		currentScore.setText("Your score : "
				+ NgageMainActivity.user.current_score);

		if (NgageMainActivity.user.current_level == Ngage.listOfLevels.levels
				.size()) {
			btnnext.setEnabled(false);
		}
	}

	public class LoadData extends AsyncTask<Void, Void, Void> {

		ProgressDialog pd;

		public LoadData(Context context) {
			pd = new ProgressDialog(context);
		}

		@Override
		protected Void doInBackground(Void... arg0) {
			// TODO Auto-generated method stub
			if (KeyValueAPI.isServerAvailable()) {
				String s = KeyValueAPI
						.get("Prachi_Pratik", "neu12345", "level_"
								+ NgageMainActivity.user.current_level + "_hs");
				if (s.equals(""))
					online_high_score = "Online High Score : Nobody Played this level yet.";
				else if (!s.contains("ERROR"))
					online_high_score = "Online High Score : " + s;
				else
					online_high_score = "Online High Score : Error fetching data.";
			} else
				online_high_score = "Online High Score : Internet Unavailable.";
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pd.dismiss();
			initializeGame();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd.setMessage("Processing...");
			pd.setIndeterminate(true);
			pd.show();
		}
	}


	public class HandleChallengeTask extends AsyncTask<Void, Void, Void> {

		ProgressDialog pd;

		public HandleChallengeTask(Context context) {
			pd = new ProgressDialog(context);
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String msg;
			NgageMainActivity.user.totalChallenges++;
			if (NgageMainActivity.user.current_score < NgageMainActivity.user.challenges
					.get(0).score) {
				msg = "Challenger "
						+ NgageMainActivity.user.challenges.get(0).username
						+ " wins";
			} else {
				msg = "Congratulations. You win.";
				NgageMainActivity.user.totalWon++;
				if (NgageMainActivity.user.scores
						.containsKey(NgageMainActivity.user.current_level)) {
					if (NgageMainActivity.user.scores
							.get(NgageMainActivity.user.current_level) < NgageMainActivity.user.current_score)
						NgageMainActivity.user.scores.put(
								NgageMainActivity.user.current_level,
								NgageMainActivity.user.current_score);
				}
			}

			Bundle data = new Bundle();
			data.putString(MESSAGE, msg);
			Message m = new Message();
			m.setData(data);
			m.what = ShowFinishChallengeDialog;
			handler.sendMessage(m);

			if (KeyValueAPI.isServerAvailable()) {
				KeyValueAPI.put("Prachi_Pratik", "neu12345",
						NgageMainActivity.user.challenges.get(0).username
								+ "_challengeScore",
						Integer.toString(NgageMainActivity.user.current_score));
				KeyValueAPI.clearKey("Prachi_Pratik", "neu12345",
						NgageMainActivity.user.username);
				Ngage.notification = true;
			} else {
				// Toast.makeText(this,
				// "No Internet Connection. Challenger cannot be notified!!",
				// Toast.LENGTH_SHORT).show();
				Bundle d = new Bundle();
				d.putString(MESSAGE,
						"No Internet Connection. Challenger cannot be notified.");
				Message m1 = new Message();
				m1.what = ShowToast;
				m1.setData(d);
				handler.sendMessage(m1);
			}

			NgageMainActivity.user.deleteChallenges();
			// a.show();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pd.dismiss();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			// TODO Auto-generated method stub
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd.setMessage("Processing...");
			pd.setIndeterminate(true);
			pd.show();
		}
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.btnShare:
			Intent is = new Intent(this, ShareOnFacebook.class);
			startActivity(is);
			break;
		case R.id.btnnext:
			if (NgageMainActivity.user != null) {
				NgageMainActivity.user.current_level++;
				// Log.d("next button", "" +
				// NgageMainActivity.user.current_level);
				Intent in = new Intent(this, NewNgageGameInstructions.class);
				startActivity(in);
			} else
				finish();
			break;
		case R.id.btnchallenge:
			// btnChallengeHandler();
			ldc = new loadDataforChallenge(FinishNgageGame.this);
			ldc.execute();
			break;
		case R.id.btnreplay:
			Intent ir = new Intent(this, NewNGageGame.class);
			startActivity(ir);
			break;
		case R.id.btnfinishgamequit:
			Intent im = new Intent(this, LevelMenu.class);
			startActivity(im);
			break;
		}

	}

	// private void btnChallengeHandler() {
	// // TODO Auto-generated method stub
	// if (KeyValueAPI.isServerAvailable())
	// {
	// String listofUsers = KeyValueAPI.get("Prachi_Pratik", "neu12345",
	// "NGAGElistofusers");
	// if (listofUsers.equals(""))
	// {
	// Toast.makeText(this, "No users available!!", Toast.LENGTH_SHORT).show();
	// }
	// else
	// {
	// String[] users = listofUsers.split(",");
	// Log.d("Number of users", ""+users.length);
	// if (users.length == 1)
	// {
	// Toast.makeText(this, "No users available!!", Toast.LENGTH_SHORT).show();
	// }
	// else
	// {
	// String[] modified_users = new String[users.length -1];
	//
	// int k=0;
	// for (int i=0; i< users.length; i++)
	// {
	// Log.d("User " + i, users[i]);
	// if (! users[i].trim().equals(NgageMainActivity.user.username))
	// {
	// modified_users[k] = users[i];
	// Log.d("User - Modified " + i, modified_users[k]);
	// k++;
	// }
	// }
	// final String[] m_users = modified_users;
	//
	// AlertDialog.Builder b = new Builder(this);
	// b.setTitle("Available users");
	// b.setItems(m_users, new DialogInterface.OnClickListener() {
	//
	// public void onClick(DialogInterface dialog, int which) {
	// // TODO Auto-generated method stub
	// dialog.dismiss();
	// handleChallenger(m_users, which);
	// }
	// });
	// b.show();
	//
	// }
	// }
	// }
	// else
	// Toast.makeText(this, "Internet not available!!",
	// Toast.LENGTH_SHORT).show();
	// }

	public class loadDataforChallenge extends AsyncTask<Void, Void, Void> {

		ProgressDialog pd;
		
		public loadDataforChallenge(Context context)
		{
			pd = new ProgressDialog(context);
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			if (KeyValueAPI.isServerAvailable()) {
				String listofUsers = KeyValueAPI.get("Prachi_Pratik",
						"neu12345", "NGAGElistofusers");
				if (listofUsers.equals("")) {
					// Toast.makeText(this, "No users available!!",
					// Toast.LENGTH_SHORT).show();
					pd.dismiss();
					Bundle d = new Bundle();
					d.putString(MESSAGE, "No users available");
					Message m1 = new Message();
					m1.what = ShowToast;
					m1.setData(d);
					handler.sendMessage(m1);
				} else {
					String[] users = listofUsers.split(",");
					Log.d("Number of users", "" + users.length);
					if (users.length == 1) {
						// Toast.makeText(this, "No users available!!",
						// Toast.LENGTH_SHORT).show();
						pd.dismiss();
						Bundle d = new Bundle();
						d.putString(MESSAGE, "No users available");
						Message m1 = new Message();
						m1.what = ShowToast;
						m1.setData(d);
						handler.sendMessage(m1);
					} else {
						String[] modified_users = new String[users.length - 1];

						int k = 0;
						for (int i = 0; i < users.length; i++) {
							Log.d("User " + i, users[i]);
							if (!users[i].trim().equals(
									NgageMainActivity.user.username)) {
								modified_users[k] = users[i];
								Log.d("User - Modified " + i, modified_users[k]);
								k++;
							}
						}
						pd.dismiss();
						Bundle data = new Bundle();
						data.putStringArray(MESSAGE, modified_users);
						Message msg = new Message();
						msg.what = ShowOnlineUsers;
						msg.setData(data);
						handler.sendMessage(msg);
					}
				}
			} else {
				// Toast.makeText(this, "Internet not available!!",
				// Toast.LENGTH_SHORT).show();
				pd.dismiss();
				Bundle d = new Bundle();
				d.putString(MESSAGE, "Internet not available.");
				Message m1 = new Message();
				m1.what = ShowToast;
				m1.setData(d);
				handler.sendMessage(m1);

			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			pd.dismiss();
			
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			pd.setMessage("Processing...");
			pd.setIndeterminate(true);
			pd.show();
		}

	}

	public void handleChallenger(String[] users, int which) {
		// TODO Auto-generated method stub
		Opponent challenged = new Opponent();
		challenged.level = NgageMainActivity.user.current_level;
		challenged.username = users[which];
		challenged.score = NgageMainActivity.user.current_score;
		if (NgageMainActivity.user.addChallenged(challenged)) {
			if (addChallengeToServer())
				Toast.makeText(this, "Opponent challenged.",
						Toast.LENGTH_SHORT).show();
			else {
				Toast.makeText(this, "No Internet Connection.",
						Toast.LENGTH_SHORT).show();
				NgageMainActivity.user.deleteChallenged();
			}
		} else
			Toast.makeText(this, "Cannot challenge more than 1 user.",
					Toast.LENGTH_SHORT).show();
		finish();
	}

	private boolean addChallengeToServer() {
		// TODO Auto-generated method stub
		if (KeyValueAPI.isServerAvailable()) {
			String opp_username = NgageMainActivity.user.challenged.get(0).username;
			int level = NgageMainActivity.user.challenged.get(0).level;
			int score = NgageMainActivity.user.challenged.get(0).score;
			KeyValueAPI.put("Prachi_Pratik", "neu12345", opp_username,
					NgageMainActivity.user.username);
			KeyValueAPI.put("Prachi_Pratik", "neu12345", opp_username
					+ "_level", Integer.toString(level));
			KeyValueAPI.put("Prachi_Pratik", "neu12345", opp_username
					+ "_score", Integer.toString(score));
			return true;
		} else {
			return false;
		}
	}
}
