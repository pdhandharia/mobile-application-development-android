package edu.neu.madcourse.pratikdhandharia;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class Boggle extends Activity implements OnClickListener{
 final Context context = this;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.boggle);
		
		//Button Click for Resume
		View btnboggleresume = (View) findViewById(R.id.btnboggleResume);
		//Log.d("Boggle.java","resume button text: " + btnboggleresume.getText());
		btnboggleresume.setOnClickListener(this);
		
		View btnbogglenewGame = (View) findViewById(R.id.btnbogglenewGame);
		btnbogglenewGame.setOnClickListener(this);
		
		View btnboggleQuit = (View) findViewById(R.id.btnboggleQuit);
		btnboggleQuit.setOnClickListener(this);
		
		View btnboggleRules = (View) findViewById(R.id.btnboggleRules);
		btnboggleRules.setOnClickListener(this);
		
		View btnboggleAcknowledgement = (View) findViewById(R.id.btnboggleAcknowledgement);
		btnboggleAcknowledgement.setOnClickListener(this);
		
		//Button Click for New Game
	}
	public void onClick(View v) {
		// TODO Auto-generated method stub
		Intent i;
		switch(v.getId())
		{
		case R.id.btnboggleResume:
			//TODO: add the PREF
			i = new Intent(this,BoggleGame.class);
			i.putExtra(BoggleGame.KEY,0);
			startActivity(i);
			break;
		case R.id.btnbogglenewGame:
			//TODO: add the PREF
			i = new Intent(this,BoggleGame.class);
			i.putExtra(BoggleGame.KEY,1);
			startActivity(i);
			break;
		case R.id.btnboggleQuit:
			onStop();
			finish();
			break;
		case R.id.btnboggleRules:
			i = new Intent(this,Rules.class);
			i.putExtra("rules", getString(R.string.descRules));
			startActivity(i);
			break;
		case R.id.btnboggleAcknowledgement:
			i = new Intent(this,Acknowledgement.class);
			i.putExtra("ack", getString(R.string.descAck));
			startActivity(i);
			break;
		}
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	
}
