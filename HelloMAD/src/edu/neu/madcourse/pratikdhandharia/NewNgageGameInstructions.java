package edu.neu.madcourse.pratikdhandharia;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

public class NewNgageGameInstructions extends Activity implements OnClickListener{

	Button btnstart,btnbackNgage;
	ImageView iv;
	TextView txtlevelno,txtminscore,txtinst;
	Level thislevel;
	
//	TableRow tr1,tr2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newngagegameinstructions);
		
		
		Display display = getWindowManager().getDefaultDisplay();
		int width = display.getWidth();
		int height = display.getHeight();
		
		iv = (ImageView) findViewById(R.id.imgshowMove);
		txtlevelno = (TextView) findViewById(R.id.txtNgageInstLevelNo);
		txtminscore = (TextView) findViewById(R.id.txtNgageInstMinScore);
		txtinst = (TextView) findViewById(R.id.txtNgageinstructions);
		
		switch((NgageMainActivity.user.current_level-1)/5)
		{
		case 0:
			iv.setBackgroundResource(R.drawable.move1animation);
			break;
		case 1:
			iv.setBackgroundResource(R.drawable.move2animation);
			break;
		case 2: 
			iv.setBackgroundResource(R.drawable.move3animation);	
			break;
		case 3:
			iv.setBackgroundResource(R.drawable.move4animation);
			break;
		}
		
		 
		 
		 thislevel = Ngage.listOfLevels.levels.get(NgageMainActivity.user.current_level-1);
		
		btnstart = (Button) findViewById(R.id.btnstartNgage);
		btnstart.setOnClickListener(this);
		btnstart.setText("Start");
		btnstart.setBackgroundResource(R.drawable.button);
		btnstart.setTextColor(Color.WHITE);
		btnstart.setWidth((int) (width*0.5));
		btnstart.setHeight(((int) (height * 0.1)));
		
		btnbackNgage = (Button) findViewById(R.id.btnbackNgage);
		btnbackNgage.setOnClickListener(this);
		btnbackNgage.setText("Back");
		btnbackNgage.setBackgroundResource(R.drawable.button);
		btnbackNgage.setTextColor(Color.WHITE);
		btnbackNgage.setWidth((int) (width*0.5));
		btnbackNgage.setHeight(((int) (height * 0.1)));
		
//		txtlevelno.setText(thislevel.level_number);
//		txtminscore.setText(thislevel.min_score_1);
		txtlevelno.setText("Level No:" + thislevel.level_number);
		txtminscore.setText("Target:" + thislevel.min_score_1);
		
		txtlevelno.setTextColor(Color.BLACK);
		txtminscore.setTextColor(Color.BLACK);
		txtinst.setTextColor(Color.BLACK);
		
		txtlevelno.setTextSize(20.0f);
		txtminscore.setTextSize(20.0f);
		txtinst.setTextSize(20.0f);
		
		txtlevelno.setWidth((int) (width*0.8));
		txtminscore.setWidth((int) (width*0.8));
		txtinst.setWidth((int) (width*0.8));
	}

	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		switch(arg0.getId())
		{
		case R.id.btnstartNgage:
			Intent i = new Intent(NewNgageGameInstructions.this, NewNGageGame.class);
			startActivity(i);
			break;
		case R.id.btnbackNgage:
			Intent ir = new Intent(NewNgageGameInstructions.this, LevelMenu.class);
			startActivity(ir);
			break;
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		// TODO Auto-generated method stub
		super.onWindowFocusChanged(hasFocus);
		AnimationDrawable frameAnimation = (AnimationDrawable) iv.getBackground();

		 // Start the animation (looped playback by default).
		 frameAnimation.start();
	}

}
